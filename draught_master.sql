-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 16-05-2017 a las 13:46:07
-- Versión del servidor: 5.7.18-0ubuntu0.16.04.1
-- Versión de PHP: 7.0.15-0ubuntu0.16.04.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `draught_master`
--

DELIMITER ;
CREATE DATABASE draught_master DEFAULT CHARACTER SET utf8 DEFAULT COLLATE utf8_general_ci;
USE draught_master;

DELIMITER $$
--
-- Funciones
--
CREATE  FUNCTION `GetBlockGrade` (`_blockId` INT, `_userId` INT) RETURNS INT(11) BEGIN
    DECLARE _grade int;

    SELECT
        SUM(grade.grade)/COUNT(m.id)
    FROM module m
    INNER JOIN (
        SELECT module_id, max(Grade) as grade
        from (
            SELECT ea.id, c.module_id, ea.grade, ea.created_at
            FROM exam_attempt ea
            INNER JOIN user_exam_answer uea on uea.exam_attempt_id = ea.id
            INNER JOIN exam_question eq on eq.id = uea.exam_question_id
            INNER JOIN content c on c.id = eq.content_id
            WHERE ea.user_id = _userId
            GROUP BY ea.id, c.module_id
            ORDER BY c.module_id, ea.grade desc, ea.created_at desc )
        temp
        GROUP BY module_id
    ) grade on grade.module_id = m.id
    WHERE m.block_id = _blockId INTO _grade;

    RETURN IF(_grade IS NULL, -1, _grade);
END$$

CREATE  FUNCTION `GetModuleGrade` (`_moduleId` INT, `_userId` INT) RETURNS INT(11) BEGIN
    DECLARE _grade int;

    SELECT max(Grade)
    from (
        SELECT ea.id, c.module_id, ea.grade, ea.created_at
        FROM exam_attempt ea
        INNER JOIN user_exam_answer uea on uea.exam_attempt_id = ea.id
        INNER JOIN exam_question eq on eq.id = uea.exam_question_id
        INNER JOIN content c on c.id = eq.content_id
        WHERE ea.user_id = _userId
        AND c.module_id = _moduleId
        GROUP BY ea.id, c.module_id
        ORDER BY c.module_id, ea.grade desc, ea.created_at desc )
    temp
    GROUP BY module_id INTO _grade;

    RETURN IF(_grade IS NULL, -1, _grade);
END$$

CREATE  FUNCTION `GetUserGrade` (`_userId` INT) RETURNS INT(11) BEGIN
    DECLARE _grade int;

    SELECT
        SUM(grade.grade)/COUNT(m.id)
    FROM module m
    INNER JOIN (
        SELECT module_id, max(Grade) as grade
        from (
            SELECT ea.id, c.module_id, ea.grade, ea.created_at
            FROM exam_attempt ea
            INNER JOIN user_exam_answer uea on uea.exam_attempt_id = ea.id
            INNER JOIN exam_question eq on eq.id = uea.exam_question_id
            INNER JOIN content c on c.id = eq.content_id
            WHERE ea.user_id = _userId
            GROUP BY ea.id, c.module_id
            ORDER BY c.module_id, ea.grade desc, ea.created_at desc )
        temp
        GROUP BY module_id
    ) grade on grade.module_id = m.id  INTO _grade;

    RETURN IF(_grade IS NULL, -1, _grade);
END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `access_type`
--

CREATE TABLE `access_type` (
  `id` int(11) NOT NULL,
  `name` varchar(45) NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `access_type`
--

INSERT INTO `access_type` (`id`, `name`, `active`) VALUES
(1, 'Course', 1),
(2, 'Block', 1),
(3, 'Module', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `block`
--

CREATE TABLE `block` (
  `id` int(11) NOT NULL,
  `name` varchar(45) NOT NULL,
  `course_id` int(11) NOT NULL,
  `init_date` datetime DEFAULT NULL,
  `end_date` datetime DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `order` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `block`
--

INSERT INTO `block` (`id`, `name`, `course_id`, `init_date`, `end_date`, `created_at`, `updated_at`, `active`, `order`) VALUES
(1, 'The welcome', 1, NULL, NULL, '2017-02-20 00:00:00', '2017-02-20 00:00:00', 1, 1),
(2, 'Warm up', 1, NULL, NULL, '2017-02-20 00:00:00', '2017-02-20 00:00:00', 1, 2),
(3, 'Master the training', 1, NULL, NULL, '2017-02-21 00:00:00', '2017-02-21 00:00:00', 1, 3),
(4, 'The evidence', 1, NULL, NULL, '2017-03-13 13:47:29', '2017-03-13 13:47:29', 1, 4),
(5, 'Follow Up', 1, NULL, NULL, '2017-03-13 13:47:51', '2017-03-13 13:47:51', 1, 5),
(6, 'Support', 1, NULL, NULL, '2017-05-02 18:39:20', '2017-05-02 18:39:20', 1, 6);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `content`
--

CREATE TABLE `content` (
  `id` int(11) NOT NULL,
  `name` varchar(45) NOT NULL,
  `module_id` int(11) NOT NULL,
  `estimated_duration` int(11) DEFAULT NULL,
  `related_content_id` int(11) DEFAULT NULL,
  `content_type_id` int(11) NOT NULL,
  `init_date` datetime DEFAULT NULL,
  `end_date` datetime DEFAULT NULL,
  `content_html` text NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `order` int(11) NOT NULL DEFAULT '0',
  `active` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `content`
--

INSERT INTO `content` (`id`, `name`, `module_id`, `estimated_duration`, `related_content_id`, `content_type_id`, `init_date`, `end_date`, `content_html`, `created_at`, `updated_at`, `order`, `active`) VALUES
(1, 'Content 1.1', 1, NULL, NULL, 1, NULL, NULL, '<section id="b1-m1-c1" class="content-text intro"> <div style="height: 50%"> <img src="main/assets/images/c/b1/m1/1.png" height="80%" alt=""> </div><div class="text"> <div> Fuiste seleccionado como candidato a certificarte como Draught Master para Heineken. </div></div><go-to-anchor delegate="module-handle" target="b1-m1-c2"></go-to-anchor></section>', '2017-04-27 19:12:45', '2017-04-27 19:12:45', 1, 1),
(2, 'Content 1.1', 1, NULL, NULL, 1, NULL, NULL, '<section id="b1-m1-c2" class="content-text"> <div class="text"> <div class="content"> <p>Como representante de Heineken, tienes la responsabilidad de una de las marcas más valiosas y admiradas del mundo. Una marca que busca la perfección en todo lo que lleve su nombre. Y tú representas ese nombre.</p><p>Para poder lograr la certificación como Draft Master tienes que ser capaz de repetir la capacitación cubriendo todos los módulos y asegurando que los objetivos de cada módulo queden claros para tu audiencia.</p></div></div><div class="image"> <img src="main/assets/images/c/b1/m1/2.png" alt=""> </div><go-to-anchor delegate="module-handle" target="b1-m1-c3"></go-to-anchor></section>', '2017-04-27 19:12:45', '2017-04-27 19:12:45', 2, 1),
(3, 'Content 1.1', 1, NULL, NULL, 2, NULL, NULL, '<section id="b1-m1-c3" class="content-video"> <div class="text"> <div class="title text-center"> Conoce de qué consta The Draught Master Training Programme. </div></div><div class="video"> <iframe src="https://player.vimeo.com/video/214909007?title=0&byline=0&portrait=0" width="100%" height="230" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe> </div><go-to-anchor delegate="module-handle" target="b1-m1-c4"></go-to-anchor></section>', '2017-04-27 19:12:45', '2017-04-27 19:12:45', 3, 1),
(4, 'Content 1.1', 1, NULL, NULL, 3, NULL, NULL, '<section id="b1-m1-c4" class="content-video-tip"> <div class="text"> <div class="content text-center"> Te presentamos a Jessica, una Draught Master que te dará recomendaciones que te serán muy útiles para desenvolverte mejor al momento de capacitar a tu audiencia. </div></div><div class="video"> <iframe src="https://player.vimeo.com/video/212985941?title=0&byline=0&portrait=0" width="100%" height="230" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe> </div></section>', '2017-04-27 19:12:45', '2017-04-27 19:12:45', 4, 1),
(5, 'Content 2.1', 2, NULL, NULL, 1, NULL, NULL, '<section id="b2-m1-c1" class="content-text intro"> <div class="text"> <div class="content"> <p class="bigger">Este módulo es para un Draught Master lo que el libro "Baldor" para un ingeniero.</p><br><p>Desempolvar tu conocimiento básico sobre la elaboración e historia de la cerveza no te tomará más de 4 minutos.</p></div></div><div class="image"> <img src="main/assets/images/c/b2/m1/1.png" alt=""> </div><go-to-anchor delegate="module-handle" target="b2-m1-c2"></go-to-anchor></section>', '2017-04-27 19:12:45', '2017-04-27 19:12:45', 1, 1),
(6, 'Content 2.1', 2, NULL, NULL, 1, NULL, NULL, '<section id="b2-m1-c2" class="content-text"> <div class="text"> <div class="title"> Primero </div><div class="content"> <p> <span class="text-bold">¿Qué es la cerveza?</span> <br>Es una bebida alcohólica que se produce cuando la levadura (hongos que producen enzimas) fermenta la cebada. </p></div></div><div class="image"> <img src="main/assets/images/c/b2/m1/2.png" alt=""> </div><go-to-anchor delegate="module-handle" target="b2-m1-c3"></go-to-anchor></section>', '2017-04-27 19:12:45', '2017-04-27 19:12:45', 2, 1),
(7, 'Content 2.1', 2, NULL, NULL, 1, NULL, NULL, '<section id="b2-m1-c3" class="content-text"> <div class="text"> <div class="title"> El nombre </div><div class="content"> <span class="text-bold text-uppercase">"CERVEZA"</span> proviene del griego "Cerevesia" por Ceres, Diosa Romana de los Cereales y frutos de la Tierra. <br/> <span class="text-bold text-uppercase">"BEER"</span> proviene del latín "bibere" que quiere decir beber. </div></div><div class="image"> <img src="main/assets/images/c/b2/m1/3.png" alt=""> </div><go-to-anchor delegate="module-handle" target="b2-m1-c4"></go-to-anchor></section>', '2017-04-27 19:12:45', '2017-04-27 19:12:45', 3, 1),
(8, 'Content 2.1', 2, NULL, NULL, 1, NULL, NULL, '<section id="b2-m1-c4" class="content-text"> <div class="text"> <div class="title"> Inicios </div><div class="content"> <p>La cerveza es la bebida mas antigua de la humanidad. <br/> Se tiene registro de su elaboración desde los primeros asentamientos humanos en el Medio Oriente hace 10 mil años. </p></div></div><div class="image"> <img src="main/assets/images/c/b2/m1/4.png" alt=""> </div><go-to-anchor delegate="module-handle" target="b2-m1-c5"></go-to-anchor></section>', '2017-04-27 19:12:45', '2017-04-27 19:12:45', 4, 1),
(9, 'Content 2.1', 2, NULL, NULL, 1, NULL, NULL, '<section id="b2-m1-c5" class="content-text"> <div class="text"> <div class="content"> <p>En la Edad Media, los Monasterios fueron los encargados de la elaboración de cerveza.</p><p>Fue hasta el siglo XVI cuando se inició con la producción en forma industrial.</p></div></div><div class="image"> <img src="main/assets/images/c/b2/m1/5.png" alt=""> </div><go-to-anchor delegate="module-handle" target="b2-m1-c6"></go-to-anchor></section>', '2017-04-27 19:12:45', '2017-04-27 19:12:45', 5, 1),
(10, 'Content 2.1', 2, NULL, NULL, 1, NULL, NULL, '<section id="b2-m1-c6" class="content-text"> <div class="text"> <div class="title"> Amada por del mundo </div><div class="content"> <p>Hoy por hoy, es la bebida alcohólica más popular del planeta. Actualmente 167 países elaboran más de 144 mil millones de litros al año.</p></div></div><div class="image"> <img src="main/assets/images/c/b2/m1/6.png" alt=""> </div><go-to-anchor delegate="module-handle" target="b2-m1-c7"></go-to-anchor></section>', '2017-04-27 19:12:45', '2017-04-27 19:12:45', 6, 1),
(11, 'Content 2.1', 2, NULL, NULL, 1, NULL, NULL, '<section id="b2-m1-c7" class="content-text"> <div class="text"> <div class="title"> Ingredientes básicos </div><div class="content"> <p> Y a pesar de que a lo largo del tiempo se han desarrollado decenas de estilos, y tipos de cerveza, los ingredientes básicos para su elaboración son solo <span class="text-bold">agua, cebada</span> y <span class="text-bold">lúpulo</span>. </p></div></div><div class="image"> <img src="main/assets/images/c/b2/m1/7.png" alt=""> </div><go-to-anchor delegate="module-handle" target="b2-m1-c8"></go-to-anchor></section>', '2017-04-27 19:12:45', '2017-04-27 19:12:45', 7, 1),
(12, 'Content 2.1', 2, NULL, NULL, 2, NULL, NULL, '<section id="b2-m1-c8" class="content-video"> <div class="text"> <div class="title"> Su historia data de hace miles años... </div><div class="content"> pero la resumimos para ti en 3 minutos. </div></div><div class="video"> <iframe src="https://player.vimeo.com/video/214934313?title=0&byline=0&portrait=0" width="100%" height="230" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe> </div><go-to-anchor delegate="module-handle" target="b2-m1-c9"></go-to-anchor></section>', '2017-04-27 19:12:45', '2017-04-27 19:12:45', 8, 1),
(13, 'Quiz 1', 2, NULL, NULL, 4, NULL, NULL, '<section id="b2-m1-c9" class="content-exam"> <div class="text"> <div class="exam-title text-uppercase"> Quiz 1 </div><div class="exam-separator"></div><div class="exam-description"> Hay miles de datos importantes que conocer de esta bebida milenaria, pero solo 5 preguntas para demostrar que la conoces lo suficiente. </div></div><div class="button-begin-exam padding"> <button class="button button-dark button-full text-uppercase" exam content="r">Empezar</button> </div></section>', '2017-04-27 19:12:45', '2017-04-27 19:12:45', 9, 1),
(14, 'Content 2.2', 3, NULL, NULL, 1, NULL, NULL, '<section id="b2-m2-c1" class="content-text intro"> <div class="text"> <div class="content"> <p class="bigger">Esta es la brevísima historia de la marca más valiosa y admirada en el mundo de la cerveza.</p><br><p>La única marca que a lo largo de sus más de 150 años de existencia ha conquistado el mundo con la cerveza de la más alta calidad.</p></div></div><div class="image"> <img src="main/assets/images/c/b2/m2/1.png" alt=""> </div><go-to-anchor delegate="module-handle" target="b2-m2-c2"></go-to-anchor></section>', '2017-04-27 19:12:45', '2017-04-27 19:12:45', 1, 1),
(15, 'Content 2.2', 3, NULL, NULL, 1, NULL, NULL, '<section id="b2-m2-c2" class="content-text"> <div class="text"> <div class="title"> EN EL PRINCIPIO FUE HEINEKEN... </div><div class="content"> <p> <span class="text-bold">1864.</span> El 14 de febrero en Amsterdam, Gerard Adriaan Heineken compra la vieja cervecería Haystack con el objetivo de producir su propia cerveza de la más alta calidad. </p></div></div><div class="image"> <img src="main/assets/images/c/b2/m2/2.png" alt=""> </div><go-to-anchor delegate="module-handle" target="b2-m2-c3"></go-to-anchor></section>', '2017-04-27 19:12:45', '2017-04-27 19:12:45', 2, 1),
(16, 'Content 2.2', 3, NULL, NULL, 1, NULL, NULL, '<section id="b2-m2-c3" class="content-text"> <div class="text"> <div class="title"> LA MEJOR CERVEZA DEL PLANETA </div><div class="content"> <p><span class="text-bold">1875.</span> Gana la Medalla de Oro en París. <br/> <span class="text-bold">1889.</span> Obtiene el “Diplome de Grand Prix” en la Feria Mundial. <br/> Distinciones que reconocen a Heineken como la mejor cerveza del mundo, mismas que podemos encontrar en la etiqueta de la botella actual.</p></div></div><div class="image"> <img src="main/assets/images/c/b2/m2/3.png" alt=""> </div><go-to-anchor delegate="module-handle" target="b2-m2-c4"></go-to-anchor></section>', '2017-04-27 19:12:45', '2017-04-27 19:12:45', 3, 1),
(17, 'Content 2.2', 3, NULL, NULL, 1, NULL, NULL, '<section id="b2-m2-c4" class="content-text"> <div class="text"> <div class="content"> <p> <span class="text-bold">1931.</span> Nuestra estrella roja nace como símbolo de la calidad Heineken. </p><p> <span class="text-bold">1933.</span> Heineken llega a América, siendo la primer cerveza europea en Estados Unidos, tan solo 3 días después de la Prohibición. </p></div></div><div class="image"> <img src="main/assets/images/c/b2/m2/4.png" alt=""> </div><go-to-anchor delegate="module-handle" target="b2-m2-c5"></go-to-anchor></section>', '2017-04-27 19:12:45', '2017-04-27 19:12:45', 4, 1),
(18, 'Content 2.2', 3, NULL, NULL, 1, NULL, NULL, '<section id="b2-m2-c5" class="content-text"> <div class="text"> <div class="content"> <p>Fue gracias a Alfred “Freddy” Heineken, que se inició con una notoria expansión por todo el mundo, misma que continúa hoy en día.</p><p><span class="text-bold">1968.</span> Heineken compra Amstel (su mayor competidor en los Países Bajos).</p><p><span class="text-bold">1974.</span> La empresa adquiere participación mayoritaria de Dreher Group.</p></div></div><div class="image"> <img src="main/assets/images/c/b2/m2/5.png" alt="" height="200"> </div><go-to-anchor delegate="module-handle" target="b2-m2-c6"></go-to-anchor></section>', '2017-04-27 19:12:45', '2017-04-27 19:12:45', 5, 1),
(19, 'Content 2.2', 3, NULL, NULL, 1, NULL, NULL, '<section id="b2-m2-c6" class="content-text"> <div class="text"> <div class="content"> <p> <span class="text-bold">2003.</span> Adquiere Brau Union en Austria, Rumania, Hungría, República CHeca y Polonia. </p><p> <span class="text-bold">2010.</span> Heineken compra Cervecería Cuauhtémoc Moctezuma </p></div></div><div class="image"> <img src="main/assets/images/c/b2/m2/6.png" alt=""> </div><go-to-anchor delegate="module-handle" target="b2-m2-c7"></go-to-anchor></section>', '2017-04-27 19:12:45', '2017-04-27 19:12:45', 6, 1),
(20, 'Content 2.2', 3, NULL, NULL, 1, NULL, NULL, '<section id="b2-m2-c7" class="content-text"> <div class="text"> <div class="content"> <p> <span class="text-bold">2014.</span> Nuestra empresa celebra su 150 aniversario. </p></div></div><div class="image"> <img src="main/assets/images/c/b2/m2/7.png" alt="" > </div><go-to-anchor delegate="module-handle" target="b2-m2-c8"></go-to-anchor></section>', '2017-04-27 19:12:45', '2017-04-27 19:12:45', 7, 1),
(21, 'Content 2.2', 3, NULL, NULL, 1, NULL, NULL, '<section id="b2-m2-c8" class="content-fact"> <div class="text"> <div class="fact"> FACT </div><div class="title"> Heineken tweaks its recipe every year. </div><div class="content"> <p> Heineken tiene una receta básica que se sigue para cada uno de sus lotes, pero con el fin de obtener exactamente el mismo sabor siempre, la receta debe ser ajustada cada año. </p><p> La cerveza necesita exactamente la misma cantidad de azúcares para su elaboración, pero como cada temporada de cultivo la cosecha es diferente, la cantidad de cebada usada debe ser ajustada. "No se trata de la receta, sino del sabor original". </p></div></div><go-to-anchor delegate="module-handle" target="b2-m2-c9"></go-to-anchor></section>', '2017-04-27 19:12:45', '2017-04-27 19:12:45', 8, 1),
(22, 'Content 2.2', 3, NULL, NULL, 2, NULL, NULL, '<section id="b2-m2-c9" class="content-video"> <div class="text"> <div class="title"> HEINEKEN EN 2 MINUTOS </div><div class="content"> Ahora este video sobre cómo el valor de nuestra marca en el mundo a lo largo del tiempo. </div></div><div class="video"> <iframe src="https://player.vimeo.com/video/214906791?title=0&byline=0&portrait=0" width="100%" height="230" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe> </div><go-to-anchor delegate="module-handle" target="b2-m2-c10"></go-to-anchor></section>', '2017-04-27 19:12:45', '2017-04-27 19:12:45', 9, 1),
(23, 'Quiz 2', 3, NULL, NULL, 4, NULL, NULL, '<section id="b2-m2-c10" class="content-exam"> <div class="text"> <div class="exam-title text-uppercase"> QUIZ 2 </div><div class="exam-separator"></div><div class="exam-description"> Un verdadero Draught Master para Heineken debe dominar esta información como si fuera de su propia familia. </div></div><div class="button-begin-exam padding"> <button class="button button-dark button-full text-uppercase" exam content="r">Empezar</button> </div></section>', '2017-04-27 19:12:45', '2017-04-27 19:12:45', 10, 1),
(24, 'Content 3.1', 4, NULL, NULL, 1, NULL, NULL, '<section id="b3-m1-c1" class="content-text intro"> <div class="text"> <div class="content"> <p class="bigger"> <span class="text-bold">Bienvenido a Draught Master Training Programme</span>, la parte medular de este curso rumbo a tu certificación.</p><br><p>A partir de ahora conocerás los secretos de las presentaciones efectivas que te permitirán ser un excelente orador Heineken </p></div></div><div class="image"> <img src="main/assets/images/c/b3/m1/1.png" alt=""> </div><go-to-anchor delegate="module-handle" target="b3-m1-c2"></go-to-anchor></section>', '2017-04-27 19:12:45', '2017-04-27 19:12:45', 1, 1),
(25, 'Content 3.1', 4, NULL, NULL, 1, NULL, NULL, '<section id="b3-m1-c2" class="content-text"> <div class="text"> <div class="title"> PRESÉNTATE </div><div class="content"> <p> Todo Draught Master sabe que la importancia de una buena presentación con los participantes es fundamental para ganarnos su atención e interés en el tema. </p><p> Empieza compartiendo información relevante sobre tu profesión; involucra a los participantes. </p></div></div><div class="image"> <img src="main/assets/images/c/b3/m1/2.png" alt=""> </div><go-to-anchor delegate="module-handle" target="b3-m1-c3"></go-to-anchor></section>', '2017-04-27 19:12:45', '2017-04-27 19:12:45', 2, 1),
(26, 'Content 3.1', 4, NULL, NULL, 1, NULL, NULL, '<section id="b3-m1-c3" class="content-text"> <div class="text"> <div class="title"> ¿DE DÓNDE PROVIENE STAR SERVE? </div><div class="content" style="line-height: 1.4"> <p>Hace 10 años, lanzamos un programa de calidad en los Países Bajos llamado "Goed Getapt" (holandés para "Well Tapped").</p><p>Hoy en día, un equipo de Draught Masters capacitan, en conjunto, 2,300 bares y restaurantes cada año. </p><p>Y tú formarás parte de este selecto grupo de capacitación bajo nuestro programa mundial: Star Serve.</p></div></div><div class="image"> <img src="main/assets/images/c/b3/m1/3.png" alt=""> </div><go-to-anchor delegate="module-handle" target="b3-m1-c4"></go-to-anchor></section>', '2017-04-27 19:12:45', '2017-04-27 19:12:45', 3, 1),
(27, 'Content 3.1', 4, NULL, NULL, 1, NULL, NULL, '<section id="b3-m1-c4" class="content-text"><!-- <div class="text"> Aqui va una imagen </div>--> <div class="image"> <img src="main/assets/images/c/b3/m1/4.png" alt=""> </div><go-to-anchor delegate="module-handle" target="b3-m1-c5"></go-to-anchor></section>', '2017-04-27 19:12:45', '2017-04-27 19:12:45', 4, 1),
(28, 'Content 3.1', 4, NULL, NULL, 2, NULL, NULL, '<section id="b3-m1-c5" class="content-video"> <div class="text"> <div class="title"> UNA INTRODUCCIÓN <br>EFECTIVA </div></div><div class="video"> <iframe src="https://player.vimeo.com/video/214057485?title=0&byline=0&portrait=0" width="100%" height="230" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe> </div><go-to-anchor delegate="module-handle" target="b3-m1-c6"></go-to-anchor></section>', '2017-04-27 19:12:45', '2017-04-27 19:12:45', 5, 1),
(29, 'Content 3.1', 4, NULL, NULL, 3, NULL, NULL, '<section id="b3-m1-c6" class="content-video-tip"> <div class="text"> <div class="title"> La primera impresión </div><div class="content"> Jessica nos comparte información importante para dirigirte a tu audiencia. </div></div><div class="video"> <iframe src="https://player.vimeo.com/video/212985990?title=0&byline=0&portrait=0" width="100%" height="230" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe> </div><go-to-anchor delegate="module-handle" target="b3-m1-c7"></go-to-anchor></section>', '2017-04-27 19:12:45', '2017-04-27 19:12:45', 6, 1),
(30, 'Content 3.1', 4, NULL, NULL, 1, NULL, NULL, '<section id="b3-m1-c7" class="content-pdf"> <div class="text"> <div class="content"> CONOCE Y MEMORIZA EL GUIÓN DE ESTE MÓDULO </div></div><div> <button-pdf content="r"></button-pdf> </div><go-to-anchor delegate="module-handle" target="b3-m1-c8"></go-to-anchor></section>', '2017-04-27 19:12:45', '2017-04-27 19:12:45', 7, 1),
(31, 'Reto 1', 4, NULL, NULL, 4, NULL, NULL, '<section id="b3-m1-c8" class="content-exam"> <div class="text"> <div class="exam-title text-uppercase"> Reto 1 </div><div class="exam-separator"></div><div class="exam-description"> Este primer reto es muy sencillo, pero requiere que tengas en cuenta las recomendaciones que has visto en los videos de este módulo. </div></div><div class="button-begin-exam padding"> <button class="button button-dark button-full text-uppercase" exam content="r" challenge="true">Empezar</button> </div></section>', '2017-04-27 19:12:45', '2017-04-27 19:12:45', 8, 1),
(32, 'Content 3.2', 5, NULL, NULL, 1, NULL, NULL, '<section id="b3-m2-c1" class="content-text intro"> <div class="text"> <div class="content"> <p class="">Al servir cerveza de barril existen 4 factores clave que si no se cuidan, pueden provocar molestias y el desagrado del consumidor.</p><p class="bigger text-bold">¿Cuales son estos 4 factores y que hacer al respecto?</p></div></div><div class="image"> <img src="main/assets/images/c/b3/m2/1.png" alt=""> </div><go-to-anchor delegate="module-handle" target="b3-m2-c2"></go-to-anchor></section>', '2017-04-27 19:12:45', '2017-04-27 19:12:45', 1, 1),
(33, 'Content 3.2', 5, NULL, NULL, 1, NULL, NULL, '<section id="b3-m2-c2" class="content-text"> <div class="text"> <div class="title"> 1. ESPUMA </div><div class="content"> <p> Poca espuma, demasiada espuma o nada de espuma, esta es una problemática constante al servir cerveza de barril. </p><p> Debemos servir la cerveza siempre con la misma cantidad de espuma. </p></div></div><div class="image"> <img src="main/assets/images/c/b3/m2/2.png" alt=""> </div><go-to-anchor delegate="module-handle" target="b3-m2-c3"></go-to-anchor></section>', '2017-04-27 19:12:45', '2017-04-27 19:12:45', 2, 1),
(34, 'Content 3.2', 5, NULL, NULL, 1, NULL, NULL, '<section id="b3-m2-c3" class="content-text"> <div class="text"> <div class="title"> 2. Temperatura </div><div class="content"> <p> Otro factor que puede causar mucha molestia en el consumidor es recibir su cerveza caliente, tibia, al tiempo, o apenas fresca. </p><p> La mejor manera de disfrutar una cerveza es que esté bien fría. </p></div></div><div class="image"> <img src="main/assets/images/c/b3/m2/3.png" alt=""> </div><go-to-anchor delegate="module-handle" target="b3-m2-c4"></go-to-anchor></section>', '2017-04-27 19:12:45', '2017-04-27 19:12:45', 3, 1),
(35, 'Content 3.2', 5, NULL, NULL, 1, NULL, NULL, '<section id="b3-m2-c4" class="content-text"> <div class="text"> <div class="title"> 3. Vasos sucios </div><div class="content"> <p> Un vaso sucio puede ocasionar una mala estructura en la espuma, además de dar la sensación de que el barman es descuidado y no pone atención en los detalles de su trabajo. </p><p> Es importante servir la cerveza en un vaso limpio, frío y bien enjuagado. </p></div></div><div class="image"> <img src="main/assets/images/c/b3/m2/4.png" height="180" alt=""> </div><go-to-anchor delegate="module-handle" target="b3-m2-c5"></go-to-anchor></section>', '2017-04-27 19:12:45', '2017-04-27 19:12:45', 4, 1),
(36, 'Content 3.2', 5, NULL, NULL, 1, NULL, NULL, '<section id="b3-m2-c5" class="content-text"> <div class="text"> <div class="title"> 4. La presentación </div><div class="content"> <p> Una cerveza entregada sin portavasos y en un vaso sin marca, comunica al cliente que la cerveza servida es ordinaria y nada especial. </p><p> Una cerveza servida con portavasos y, en cristalería de la marca, incrementa la satisfacción del cliente, y asegura una presentación de calidad. </p></div></div><div class="image"> <img src="main/assets/images/c/b3/m2/5.png" alt=""> </div><go-to-anchor delegate="module-handle" target="b3-m2-c6"></go-to-anchor></section>', '2017-04-27 19:12:45', '2017-04-27 19:12:45', 5, 1),
(37, 'Content 3.2', 5, NULL, NULL, 2, NULL, NULL, '<section id="b3-m2-c6" class="content-video"> <div class="text"> <div class="title"> LAS MOLESTIAS DOMINADAS POR UN DRAUGHT MASTER </div></div><div class="video"> <iframe src="https://player.vimeo.com/video/214057299?title=0&byline=0&portrait=0" width="100%" height="230" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe> </div><go-to-anchor delegate="module-handle" target="b3-m2-c7"></go-to-anchor></section>', '2017-04-27 19:12:45', '2017-04-27 19:12:45', 6, 1),
(38, 'Content 3.2', 5, NULL, NULL, 3, NULL, NULL, '<section id="b3-m2-c7" class="content-video-tip"> <div class="text"> <div class="title"> TIP: Recapitular todas las molestias. </div></div><div class="video"> <iframe src="https://player.vimeo.com/video/212986001?title=0&byline=0&portrait=0" width="100%" height="230" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe> </div><go-to-anchor delegate="module-handle" target="b3-m2-c8"></go-to-anchor></section>', '2017-04-27 19:12:45', '2017-04-27 19:12:45', 7, 1),
(39, 'Content 3.2', 5, NULL, NULL, 1, NULL, NULL, '<section id="b3-m2-c8" class="content-pdf"> <div class="text"> <div class="content"> CONOCE Y MEMORIZA EL GUIÓN DE ESTE MÓDULO </div></div><div> <button-pdf content="r"></button-pdf> </div><go-to-anchor delegate="module-handle" target="b3-m2-c9"></go-to-anchor></section>', '2017-04-27 19:12:45', '2017-04-27 19:12:45', 8, 1),
(40, 'Quiz 4', 5, NULL, NULL, 4, NULL, NULL, '<section id="b3-m2-c9" class="content-exam"> <div class="text"> <div class="exam-title text-uppercase"> QUIZ 4 </div><div class="exam-separator"></div><div class="exam-description"> Como consumidor las irritaciones no las toleramos, como futuro Draught Master debes saber cómo evitarlas. </div></div><div class="button-begin-exam padding"> <button class="button button-dark button-full text-uppercase" exam content="r">Empezar</button> </div></section>', '2017-04-27 19:12:45', '2017-04-27 19:12:45', 9, 1),
(41, 'Content 3.3', 6, NULL, NULL, 1, NULL, NULL, '<section id="b3-m3-c1" class="content-text intro"> <div class="text"> <div class="content"> <p> Porque servir una cerveza de barril es muy fácil… ¿O no? </p><p class="text-bold bigger"> Vamos a comprobarlo </p></div></div><div class="image"> <img src="main/assets/images/c/b3/m3/1.png" alt="" height="190"> </div><go-to-anchor delegate="module-handle" target="b3-m3-c2"></go-to-anchor></section>', '2017-04-27 19:12:45', '2017-04-27 19:12:45', 1, 1),
(42, 'Content 3.3', 6, NULL, NULL, 1, NULL, NULL, '<section id="b3-m3-c2" class="content-text"> <div class="text"> <div class="title"> CERVEZA DE REFERENCIA </div><div class="content"> <p> La cerveza de referencia es la primer cerveza que servimos directo del grifo. Nuestra cerveza de prueba. </p><p> Regularmente el servido de esta cerveza no es el mejor. Debemos tomar esta cerveza como referencia y analizar todos los fallos, para lograr un mejor resultado en el próximo servido. </p></div></div><div class="image"> <img src="main/assets/images/c/b3/m3/2.png" alt="" height="180"> </div><go-to-anchor delegate="module-handle" target="b3-m3-c3"></go-to-anchor></section>', '2017-04-27 19:12:45', '2017-04-27 19:12:45', 2, 1),
(43, 'Content 3.3', 6, NULL, NULL, 1, NULL, NULL, '<section id="b3-m3-c3" class="content-text"> <div class="text"> <div class="title"> CONSIDERAR </div><div class="content"> <p> Enjuagar el vaso, la forma de abrir y cerrar el grifo, el ángulo del vaso y la manera de usar el skimmer, todo esto afecta el resultado final. </p><p> El ritual Star Serve pone orden a la forma de serivir y asegura obtener siempre una cerveza perfecta.. </p></div></div><div class="image"> <img src="main/assets/images/c/b3/m3/3.png" alt=""> </div><go-to-anchor delegate="module-handle" target="b3-m3-c4"></go-to-anchor></section>', '2017-04-27 19:12:45', '2017-04-27 19:12:45', 3, 1),
(44, 'Content 3.3', 6, NULL, NULL, 2, NULL, NULL, '<section id="b3-m3-c4" class="content-video"> <div class="text"> <div class="title"> QUE ANALIZAR DE NUESTRA CERVEZA DE REFERENCIA </div></div><div class="video"> <iframe src="https://player.vimeo.com/video/214057239?title=0&byline=0&portrait=0" width="100%" height="230" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe> </div><go-to-anchor delegate="module-handle" target="b3-m3-c5"></go-to-anchor></section>', '2017-04-27 19:12:45', '2017-04-27 19:12:45', 4, 1),
(45, 'Content 3.3', 6, NULL, NULL, 3, NULL, NULL, '<section id="b3-m3-c5" class="content-video-tip"> <div class="text"> <div class="title"> TIP: NO OLVIDES FIJAR UN OBJETIVO PARA TU SEGUNDA CERVEZA </div></div><div class="video"> <iframe src="https://player.vimeo.com/video/212986030?title=0&byline=0&portrait=0" width="100%" height="230" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe> </div><go-to-anchor delegate="module-handle" target="b3-m3-c6"></go-to-anchor></section>', '2017-04-27 19:12:45', '2017-04-27 19:12:45', 5, 1),
(46, 'Content 3.3', 6, NULL, NULL, 1, NULL, NULL, '<section id="b3-m3-c6" class="content-pdf"> <div class="text"> <div class="content"> CONOCE Y MEMORIZA EL GUIÓN DE ESTE MÓDULO </div></div><div> <button-pdf content="r"></button-pdf> </div><go-to-anchor delegate="module-handle" target="b3-m3-c7"></go-to-anchor></section>', '2017-04-27 19:12:45', '2017-04-27 19:12:45', 6, 1),
(47, 'Quiz 5', 6, NULL, NULL, 4, NULL, NULL, '<section id="b3-m3-c7" class="content-exam"> <div class="text"> <div class="exam-title text-uppercase"> QUIZ 5 </div><div class="exam-separator"></div><div class="exam-description"> Ahora que conoces la importancia de la cerveza de referencia, estas listo para contestar... </div></div><div class="button-begin-exam padding"> <button class="button button-dark button-full text-uppercase" exam content="r">Empezar</button> </div></section>', '2017-04-27 19:12:45', '2017-04-27 19:12:45', 7, 1),
(48, 'Content 3.4', 7, NULL, NULL, 1, NULL, NULL, '<section id="b3-m4-c1" class="content-text intro"> <div class="text"> <div class="content"> <p class="bigger text-bold">El ritual de servir Star Serve es muy simple. Pero, ¿Por qué debe hacerlo?</p><p class="">Porque usted entiende que con este ritual va a aumentar la tomabilidad de la cervez y va a aumentar las ventas y el volumen.</p></div></div><div class="image"> <img src="main/assets/images/c/b3/m4/1.png" height="180" alt=""> </div><go-to-anchor delegate="module-handle" target="b3-m4-c2"></go-to-anchor></section>', '2017-04-27 19:12:45', '2017-04-27 19:12:45', 1, 1),
(49, 'Content 3.4', 7, NULL, NULL, 1, NULL, NULL, '<section id="b3-m4-c2" class="content-text"> <div class="text"> <div class="title"> Enjuague </div><div class="content"> <p>Al enjuagar el vaso también lo enfría. Una cristalería limpia y fría mantendrá una cerveza fría por más tiempo. El agua debe adherirse al tarro.</p><p>Después de lavar, sostenga el vaso al revés para drenar exceso de agua.</p></div></div><div class="image"> <img src="main/assets/images/c/b3/m4/2.png" alt=""> </div><go-to-anchor delegate="module-handle" target="b3-m4-c3"></go-to-anchor></section>', '2017-04-27 19:12:45', '2017-04-27 19:12:45', 2, 1),
(50, 'Content 3.4', 7, NULL, NULL, 1, NULL, NULL, '<section id="b3-m4-c3" class="content-text"> <div class="text"> <div class="title"> VERTIR </div><div class="content"> <p>Abra el grifo con un movimiento suave y deje vaciar la primera gota. Deje caer la cerveza en esperial en el vaso, sosteniéndolo en un ángulo de 45 grados.</p><p>Al punto de llenarse enderezca el vaso y cierre el grifo antes de que empiece a derramar la espuma.</p></div></div><div class="image"> <img src="main/assets/images/c/b3/m4/3.png" height="180" alt=""> </div><go-to-anchor delegate="module-handle" target="b3-m4-c4"></go-to-anchor></section>', '2017-04-27 19:12:45', '2017-04-27 19:12:45', 3, 1),
(51, 'Content 3.4', 7, NULL, NULL, 1, NULL, NULL, '<section id="b3-m4-c4" class="content-text"> <div class="text"> <div class="title"> Corte </div><div class="content"> <p>Al empezar a desbordarse corte la espuma con un skimmer húmedo en un ángulo de 45 grados.</p><p>Removiendo exceso de espuma, dejando una delgada capa de agua brillante.</p></div></div><div class="image"> <img src="main/assets/images/c/b3/m4/4.png" alt=""> </div><go-to-anchor delegate="module-handle" target="b3-m4-c5"></go-to-anchor></section>', '2017-04-27 19:12:45', '2017-04-27 19:12:45', 4, 1),
(52, 'Content 3.4', 7, NULL, NULL, 1, NULL, NULL, '<section id="b3-m4-c5" class="content-text"> <div class="text"> <div class="title"> Revisar </div><div class="content"> <p>Espere que la espuma se estabilice y observe que la línea de la espuma descansa de manera horizontal sobre los hombros de la estrella.</p></div></div><div class="image"> <img src="main/assets/images/c/b3/m4/5.png" alt=""> </div><go-to-anchor delegate="module-handle" target="b3-m4-c6"></go-to-anchor></section>', '2017-04-27 19:12:45', '2017-04-27 19:12:45', 5, 1),
(53, 'Content 3.4', 7, NULL, NULL, 1, NULL, NULL, '<section id="b3-m4-c6" class="content-text"> <div class="text"> <div class="title"> Presentar </div><div class="content"> <p>Mientras sostiene el vaso por la base, preséntelo en su portavasos con el logo mirando al cliente, haciendo contacto visual y diciendo “disfrute su Heineken”.</p><p>Expertos coinciden en que este ritual de servir es lo mejor para este producto.</p></div></div><div class="image"> <img src="main/assets/images/c/b3/m4/6.png" alt=""> </div><go-to-anchor delegate="module-handle" target="b3-m4-c7"></go-to-anchor></section>', '2017-04-27 19:12:45', '2017-04-27 19:12:45', 6, 1),
(54, 'Content 3.4', 7, NULL, NULL, 2, NULL, NULL, '<section id="b3-m4-c7" class="content-video"> <div class="text"> <div class="title"> VER A UN DRAUGHT MASTER HACER EL RITUAL ES ALGO QUE SOLO OTROS DRAUGHTS SABEN APRECIAR </div></div><div class="video"> <iframe src="https://player.vimeo.com/video/214057165?title=0&byline=0&portrait=0" width="100%" height="230" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe> </div><go-to-anchor delegate="module-handle" target="b3-m4-c8"></go-to-anchor></section>', '2017-04-27 19:12:45', '2017-04-27 19:12:45', 7, 1),
(55, 'Content 3.4', 7, NULL, NULL, 3, NULL, NULL, '<section id="b3-m4-c8" class="content-video-tip"> <div class="text"> <div class="title"> TIP: DEMUESTRA QUE EL RITUAL ES SENCILLO Y FÁCIL DE EJECUTAR </div></div><div class="video"> <iframe src="https://player.vimeo.com/video/212986058?title=0&byline=0&portrait=0" width="100%" height="200" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe> </div><go-to-anchor delegate="module-handle" target="b3-m4-c9"></go-to-anchor></section>', '2017-04-27 19:12:45', '2017-04-27 19:12:45', 8, 1),
(56, 'Content 3.4', 7, NULL, NULL, 1, NULL, NULL, '<section id="b3-m4-c9" class="content-pdf"> <div class="text"> <div class="content"> CONOCE Y MEMORIZA EL GUIÓN DE ESTE MÓDULO </div></div><div> <button-pdf content="r"></button-pdf> </div><go-to-anchor delegate="module-handle" target="b3-m4-c10"></go-to-anchor></section>', '2017-04-27 19:12:45', '2017-04-27 19:12:45', 9, 1),
(57, 'Quiz 6', 7, NULL, NULL, 4, NULL, NULL, '<section id="b3-m4-c10" class="content-exam"> <div class="text"> <div class="exam-title text-uppercase"> QUIZ 6 </div><div class="exam-separator"></div><div class="exam-description"> La práctica hace al maestro, pero todo maestro debe dominar la teoría. Demuestra que dominas El Ritual. </div></div><div class="button-begin-exam padding"> <button class="button button-dark button-full text-uppercase" exam content="r">Empezar</button> </div></section>', '2017-04-27 19:12:45', '2017-04-27 19:12:45', 10, 1),
(58, 'Content 3.5', 8, NULL, NULL, 1, NULL, NULL, '<section id="b3-m5-c1" class="content-text intro"> <div class="text"> <div class="title"> <p class="bigger"> ¿Porqué es tan importante el ritual de Star Serve? </p></div><div class="content"> <p> El cliente espera una bebida perfecta. La espuma es la clave para asegurar la calidad en el sabor de la cerveza. </p></div></div><div class="image"> <img src="main/assets/images/c/b3/m5/1.png" alt="" height="190"> </div><go-to-anchor delegate="module-handle" target="b3-m5-c2"></go-to-anchor></section>', '2017-04-27 19:12:45', '2017-04-27 19:12:45', 1, 1),
(59, 'Content 3.5', 8, NULL, NULL, 1, NULL, NULL, '<section id="b3-m5-c2" class="content-text"> <div class="text"> <div class="title"> LA QUÍMICA EN MI CERVEZA </div><div class="content"> <p> Mientras tú sigues el ritual de Star Serve, suceden efectos químicos de oxidación que definen el sabor del líquido que probará el cliente. </p><p> Existe un factor clave para controlar este proceso químico: la espuma. </p></div></div><div class="image"> <img src="main/assets/images/c/b3/m5/2.png" alt="" height="180"> </div><go-to-anchor delegate="module-handle" target="b3-m5-c3"></go-to-anchor></section>', '2017-04-27 19:12:45', '2017-04-27 19:12:45', 2, 1),
(60, 'Content 3.5', 8, NULL, NULL, 1, NULL, NULL, '<section id="b3-m5-c3" class="content-text"> <div class="text"> <div class="title"> OXIDACIÓN </div><div class="content"> <p> Cuando servimos la cerveza, el CO2 trata de escapar del líquido, y el oxigeno trata de entrar. </p><p> Si permitimos que esto sucede nuestra cerveza tendrá un sabor que al cliente no le gustará. </p></div></div><div class="image"> <img src="main/assets/images/c/b3/m5/3.png" alt=""> </div><go-to-anchor delegate="module-handle" target="b3-m5-c4"></go-to-anchor></section>', '2017-04-27 19:12:45', '2017-04-27 19:12:45', 3, 1),
(61, 'Content 3.5', 8, NULL, NULL, 1, NULL, NULL, '<section id="b3-m5-c4" class="content-text"> <div class="text"> <div class="title"> FUNCIÓN DE LA ESPUMA </div><div class="content"> <p> La espuma en nuestro vaso brinda protección a nuestra cerveza y asegura la calidad del sabor. </p><p> La espuma evita que nuestra cerveza pierda el CO2 e impide que el oxígeno entre al líquido. </p></div></div><div class="image"> <img src="main/assets/images/c/b3/m5/4.png" alt=""> </div><go-to-anchor delegate="module-handle" target="b3-m5-c5"></go-to-anchor></section>', '2017-04-27 19:12:45', '2017-04-27 19:12:45', 4, 1),
(62, 'Content 3.5', 8, NULL, NULL, 2, NULL, NULL, '<section id="b3-m5-c5" class="content-video"> <div class="text"> <div class="title"> LA IMPORTANCIA DE LA CABEZA DE NUESTRA CERVEZA </div></div><div class="video"> <iframe src="https://player.vimeo.com/video/214057039?title=0&byline=0&portrait=0" width="100%" height="230" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe> </div><go-to-anchor delegate="module-handle" target="b3-m5-c6"></go-to-anchor></section>', '2017-04-27 19:12:45', '2017-04-27 19:12:45', 5, 1),
(63, 'Content 3.5', 8, NULL, NULL, 3, NULL, NULL, '<section id="b3-m5-c6" class="content-video-tip"> <div class="text"> <div class="title"> TIP: UTILIZA UNA BOTELLA CERRADA PARA EXPLICAR LA FUNCIÓN DE PROTECCIÓN DE LA ESPUMA </div></div><div class="video"> <iframe src="https://player.vimeo.com/video/212986094?title=0&byline=0&portrait=0" width="100%" height="230" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe> </div><go-to-anchor delegate="module-handle" target="b3-m5-c7"></go-to-anchor></section>', '2017-04-27 19:12:45', '2017-04-27 19:12:45', 6, 1),
(64, 'Content 3.5', 8, NULL, NULL, 1, NULL, NULL, '<section id="b3-m5-c7" class="content-pdf"> <div class="text"> <div class="content"> CONOCE Y MEMORIZA EL GUIÓN DE ESTE MÓDULO </div></div><div> <button-pdf content="r"></button-pdf> </div><go-to-anchor delegate="module-handle" target="b3-m5-c8"></go-to-anchor></section>', '2017-04-27 19:12:45', '2017-04-27 19:12:45', 7, 1),
(65, 'Quiz 7', 8, NULL, NULL, 4, NULL, NULL, '<section id="b3-m5-c8" class="content-exam"> <div class="text"> <div class="exam-title text-uppercase"> QUIZ 7 </div><div class="exam-separator"></div><div class="exam-description"> Un buen Draught Master no solo realiza el ritual Star Serve, sino que entiende lo que está pasando con la cerveza al momento de servir. Demuestra que ya tienes este conocimiento. </div></div><div class="button-begin-exam padding"> <button class="button button-dark button-full text-uppercase" exam content="r">Empezar</button> </div></section>', '2017-04-27 19:12:45', '2017-04-27 19:12:45', 8, 1),
(66, 'Content 3.6', 9, NULL, NULL, 1, NULL, NULL, '<section id="b3-m6-c1" class="content-text intro"> <div class="text"> <div class="content"> <p class="bigger text-bold">¿Por qué beber cerveza? <br>¿Cuánto tiempo tenemos bebiendo cerveza?</p><p class="">La cerveza es una bebida muy antigua, y la razón por la que comenzamos a beber cerveza no es precisamente para emborracharnos.</p></div></div><div class="image"> <img src="main/assets/images/c/b3/m6/1.png" height="180" alt=""> </div><go-to-anchor delegate="module-handle" target="b3-m6-c2"></go-to-anchor></section>', '2017-04-27 19:12:45', '2017-04-27 19:12:45', 1, 1),
(67, 'Content 3.6', 9, NULL, NULL, 1, NULL, NULL, '<section id="b3-m6-c2" class="content-text"> <div class="text"> <div class="title"> Quitased </div><div class="content"> <p>La historia de la cerveza comienza hace 10 mil años, cuando surgió la necesidad de una bebida quita sed ya que el agua regularmente no era limpia.</p><p>Y sabemos que una bebida quitased tiene que poseer una característica muy importante: debe ser LÍQUIDA, FRÍA, CON UN TOQUE AMARGO Y CON CO2.</p></div></div><div class="image"> <img src="main/assets/images/c/b3/m6/2.png" height="180" alt=""> </div><go-to-anchor delegate="module-handle" target="b3-m6-c3"></go-to-anchor></section>', '2017-04-27 19:12:45', '2017-04-27 19:12:45', 2, 1),
(68, 'Content 3.6', 9, NULL, NULL, 1, NULL, NULL, '<section id="b3-m6-c3" class="content-text"> <div class="text"> <div class="title"> Refrescante </div><div class="content"> <p>Lo que hace que nuestra cerveza sea refrescante es el CO2. Por eso es tan importante que durante el servido de la cerveza, el CO2 no se escape.</p></div></div><div class="image"> <img src="main/assets/images/c/b3/m6/3.png" height="200" alt=""> </div><go-to-anchor delegate="module-handle" target="b3-m6-c4"></go-to-anchor></section>', '2017-04-27 19:12:45', '2017-04-27 19:12:45', 3, 1),
(69, 'Content 3.6', 9, NULL, NULL, 1, NULL, NULL, '<section id="b3-m6-c4" class="content-text"> <div class="text"> <div class="title"> M&aacute;s ventas </div><div class="content"> <p>Otro de los efectos que tiene el CO2 es que es fácil de digerir y al contrario de bebidas no carbonatadas que nos dan la sensación de estar llenos, la cerveza se digiere fácilmente y entonces el consumidor puede tomar más producto. Claro siempre de manera responsable.</p></div></div><div class="image"> <img src="main/assets/images/c/b3/m6/4.png" height="200" alt=""> </div><go-to-anchor delegate="module-handle" target="b3-m6-c5"></go-to-anchor></section>', '2017-04-27 19:12:45', '2017-04-27 19:12:45', 4, 1),
(70, 'Content 3.6', 9, NULL, NULL, 2, NULL, NULL, '<section id="b3-m6-c5" class="content-video"> <div class="text"> <div class="title"> LA CERVEZA MÁS <br>REFRESCANTE </div></div><div class="video"> <iframe src="https://player.vimeo.com/video/214053214?title=0&byline=0&portrait=0" width="100%" height="230" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe> </div><go-to-anchor delegate="module-handle" target="b3-m6-c6"></go-to-anchor></section>', '2017-04-27 19:12:45', '2017-04-27 19:12:45', 5, 1),
(71, 'Content 3.6', 9, NULL, NULL, 3, NULL, NULL, '<section id="b3-m6-c6" class="content-video-tip"> <div class="text"> <div class="title"> TIP: ASEGÚRATE QUE TODOS PARTICIPEN Y ENTIENDAN, PREGUNTA FRECUENTEMENTE </div></div><div class="video"> <iframe src="https://player.vimeo.com/video/212986125?title=0&byline=0&portrait=0" width="100%" height="200" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe> </div><go-to-anchor delegate="module-handle" target="b3-m6-c7"></go-to-anchor></section>', '2017-04-27 19:12:45', '2017-04-27 19:12:45', 6, 1),
(72, 'Content 3.6', 9, NULL, NULL, 1, NULL, NULL, '<section id="b3-m6-c7" class="content-pdf"> <div class="text"> <div class="content"> CONOCE Y MEMORIZA EL GUIÓN DE ESTE MÓDULO </div></div><div> <button-pdf content="r"></button-pdf> </div><go-to-anchor delegate="module-handle" target="b3-m6-c8"></go-to-anchor></section>', '2017-04-27 19:12:45', '2017-04-27 19:12:45', 7, 1),
(73, 'Quiz 8', 9, NULL, NULL, 4, NULL, NULL, '<section id="b3-m6-c8" class="content-exam"> <div class="text"> <div class="exam-title text-uppercase"> QUIZ 8 </div><div class="exam-separator"></div><div class="exam-description"> Ahora tú ya sabes hace cuánto se inventó la cerveza y porque. Contesta el siguiente quiz y demuestra tus conocimientos. </div></div><div class="button-begin-exam padding"> <button class="button button-dark button-full text-uppercase" exam content="r">Empezar</button> </div></section>', '2017-04-27 19:12:45', '2017-04-27 19:12:45', 8, 1),
(74, 'Content 3.7', 10, NULL, NULL, 1, NULL, NULL, '<section id="b3-m7-c1" class="content-text intro"> <div class="text"> <div class="title"> ¿Cómo podemos identificar a un experto bebedor de cerveza? ¿Cómo podemos vender más cerveza? </div><div class="content"> <p> El regusto, o “after taste” que genera la cerveza es factor clave para que el consumidor quiera pedir más. </p></div></div><div class="image"> <img src="main/assets/images/c/b3/m7/1.png" alt="" height="190"> </div><go-to-anchor delegate="module-handle" target="b3-m7-c2"></go-to-anchor></section>', '2017-04-27 19:12:45', '2017-04-27 19:12:45', 1, 1),
(75, 'Content 3.7', 10, NULL, NULL, 1, NULL, NULL, '<section id="b3-m7-c2" class="content-text"> <div class="text"> <div class="title"> LA PERCHA </div><div class="content"> <p> Un verdadero bebedor de cerveza saber que la espuma es la protección del líquido, por lo tanto al tomar del vaso, no le dará un sorbo a la espuma. Sino que inclinará el vaso para que el líquido escape por abajo de la espuma. </p><p> Esto ayudará a que su cerveza se mantenga fresca y con más CO2 por más tiempo. </p></div></div><div class="image"> <img src="main/assets/images/c/b3/m7/2.png" alt="" height="180"> </div><go-to-anchor delegate="module-handle" target="b3-m7-c3"></go-to-anchor></section>', '2017-04-27 19:12:45', '2017-04-27 19:12:45', 2, 1),
(76, 'Content 3.7', 10, NULL, NULL, 1, NULL, NULL, '<section id="b3-m7-c3" class="content-text"> <div class="text"> <div class="title"> RETROGUSTO </div><div class="content"> <p> A diferencia de otras bebidas, la cerveza no se degusta por mucho tiempo en la boca, sino que se pasa rápidamente. </p><p> Y entonces el CO2 se activa en el cuerpo y genera una necesidad de dar un trago más. </p></div></div><div class="image"> <img src="main/assets/images/c/b3/m7/3.png" alt="" height="180"> </div><go-to-anchor delegate="module-handle" target="b3-m7-c4"></go-to-anchor></section>', '2017-04-27 19:12:45', '2017-04-27 19:12:45', 3, 1),
(77, 'Content 3.7', 10, NULL, NULL, 1, NULL, NULL, '<section id="b3-m7-c4" class="content-text"> <div class="text"> <div class="title"> REPETICIÓN </div><div class="content"> <p> Una cerveza fría, presentada en un vaso limpio, con la cantidad correcta de espuma, y no oxidada, incrementará las posibilidades de que el consumidor pida otra. </p><p> Por el contrario una cerveza “plana” o sin CO2 dará la sensación de hinchazón y el consumidor no querrá seguir bebiendo. </p></div></div><div class="image"> <img src="main/assets/images/c/b3/m7/4.png" alt="" height="180"> </div><go-to-anchor delegate="module-handle" target="b3-m7-c5"></go-to-anchor></section>', '2017-04-27 19:12:46', '2017-04-27 19:12:46', 4, 1),
(78, 'Content 3.7', 10, NULL, NULL, 2, NULL, NULL, '<section id="b3-m7-c5" class="content-video"> <div class="text"> <div class="title"> UN DRAUGHT MASTER NOS DICE CÓMO DISFRUTAR UNA CERVEZA </div></div><div class="video"> <iframe src="https://player.vimeo.com/video/214052868?title=0&byline=0&portrait=0" width="100%" height="230" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe> </div><go-to-anchor delegate="module-handle" target="b3-m7-c6"></go-to-anchor></section>', '2017-04-27 19:12:46', '2017-04-27 19:12:46', 5, 1),
(79, 'Content 3.7', 10, NULL, NULL, 3, NULL, NULL, '<section id="b3-m7-c6" class="content-video-tip"> <div class="text"> <div class="title"> TIP: LA SEGUNDA CERVEZA DEPENDE DE LA CALIDAD DE LA PRIMERA </div></div><div class="video"> <iframe src="https://player.vimeo.com/video/212986149?title=0&byline=0&portrait=0" width="100%" height="230" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe> </div><go-to-anchor delegate="module-handle" target="b3-m7-c7"></go-to-anchor></section>', '2017-04-27 19:12:46', '2017-04-27 19:12:46', 6, 1),
(80, 'Content 3.7', 10, NULL, NULL, 1, NULL, NULL, '<section id="b3-m7-c7" class="content-pdf"> <div class="text"> <div class="content"> CONOCE Y MEMORIZA EL GUIÓN DE ESTE MÓDULO </div></div><div> <button-pdf content="r"></button-pdf> </div><go-to-anchor delegate="module-handle" target="b3-m7-c8"></go-to-anchor></section>', '2017-04-27 19:12:46', '2017-04-27 19:12:46', 7, 1),
(81, 'Quiz 9', 10, NULL, NULL, 4, NULL, NULL, '<section id="b3-m7-c8" class="content-exam"> <div class="text"> <div class="exam-title text-uppercase"> QUIZ 9 </div><div class="exam-separator"></div><div class="exam-description"> Ahora que ya sabes por qué se disfruta tanto de una cerveza, demuestra que también disfrutas aprendiendo. </div></div><div class="button-begin-exam padding"> <button class="button button-dark button-full text-uppercase" exam content="r">Empezar</button> </div></section>', '2017-04-27 19:12:46', '2017-04-27 19:12:46', 8, 1),
(82, 'Content 3.8', 11, NULL, NULL, 1, NULL, NULL, '<section id="b3-m8-c1" class="content-text intro"> <div class="text"> <div class="content"> <p class="bigger text-bold">¿Qué tan importante es cortar la cerveza?<br>¿Pasa algo si no lo hago?</p><p class="">¡El sabor de una cerveza cortada es lo que el consumidor busca! No olvidemos este importante paso del Star Serve.</p></div></div><div class="image"> <img src="main/assets/images/c/b3/m8/1.png" height="180" alt=""> </div><go-to-anchor delegate="module-handle" target="b3-m8-c2"></go-to-anchor></section>', '2017-04-27 19:12:46', '2017-04-27 19:12:46', 1, 1),
(83, 'Content 3.8', 11, NULL, NULL, 1, NULL, NULL, '<section id="b3-m8-c2" class="content-text"> <div class="text"> <div class="title"> Cerveza no cortada </div><div class="content"> <p>Cuando servimos una cerveza no cortada la espuma se oxigena más rápidamente, por lo que lo que se forman burbujas desiguales que ponen en riesgo la estructura de la cabeza de espuma.</p><p>Además el amargor del lúpulo se queda en la espuma y termina por asentarse en la cerveza.</p></div></div><div class="image"> <img src="main/assets/images/c/b3/m8/2.png" height="180" alt=""> </div><go-to-anchor delegate="module-handle" target="b3-m8-c3"></go-to-anchor></section>', '2017-04-27 19:12:46', '2017-04-27 19:12:46', 2, 1),
(84, 'Content 3.8', 11, NULL, NULL, 1, NULL, NULL, '<section id="b3-m8-c3" class="content-text"> <div class="text"> <div class="title"> Cerveza cortada </div><div class="content"> <p>Al cortar la cerveza, lo que hacemos es remover la espuma con mayor concentración de lúpulo (la espuma más amarga).</p><p>Además con el skimmer mojado creamos una ligera capa de agua que protege nuestra espuma y asegura frescura por mayor tiempo.</p></div></div><div class="image"> <img src="main/assets/images/c/b3/m8/3.png" height="200" alt=""> </div><go-to-anchor delegate="module-handle" target="b3-m8-c4"></go-to-anchor></section>', '2017-04-27 19:12:46', '2017-04-27 19:12:46', 3, 1);
INSERT INTO `content` (`id`, `name`, `module_id`, `estimated_duration`, `related_content_id`, `content_type_id`, `init_date`, `end_date`, `content_html`, `created_at`, `updated_at`, `order`, `active`) VALUES
(85, 'Content 3.8', 11, NULL, NULL, 1, NULL, NULL, '<section id="b3-m8-c4" class="content-text"> <div class="text"> <div class="title"> La cerveza perfecta </div><div class="content"> <p class="text-bold">Cortar la cerveza es un paso muy importante en el ritual de Star Serve.</p><p class="text-bold">Para servir la cerveza perfecta debemos cuidar que el consumidor no beba todo el amargor del lúpulo en el primer sorbo.</p></div></div><div class="image"> <img src="main/assets/images/c/b3/m8/4.png" height="200" alt=""> </div><go-to-anchor delegate="module-handle" target="b3-m8-c5"></go-to-anchor></section>', '2017-04-27 19:12:46', '2017-04-27 19:12:46', 4, 1),
(86, 'Content 3.8', 11, NULL, NULL, 2, NULL, NULL, '<section id="b3-m8-c5" class="content-video"> <div class="text"> <div class="title"> COMO DESNATAR UNA CERVEZA DE MANERA PERFECTA </div></div><div class="video"> <iframe src="https://player.vimeo.com/video/214052784?title=0&byline=0&portrait=0" width="100%" height="230" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe> </div><go-to-anchor delegate="module-handle" target="b3-m8-c6"></go-to-anchor></section>', '2017-04-27 19:12:46', '2017-04-27 19:12:46', 5, 1),
(87, 'Content 3.8', 11, NULL, NULL, 3, NULL, NULL, '<section id="b3-m8-c6" class="content-video-tip"> <div class="text"> <div class="title"> TIP: RECUERDA USAR TU SKIMMER MOJADO Y CORTAR A UN ÁNGULO DE 45 GRADOS. </div></div><div class="video"> <iframe src="https://player.vimeo.com/video/212986171?title=0&byline=0&portrait=0" width="100%" height="200" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe> </div><go-to-anchor delegate="module-handle" target="b3-m8-c7"></go-to-anchor></section>', '2017-04-27 19:12:46', '2017-04-27 19:12:46', 6, 1),
(88, 'Content 3.8', 11, NULL, NULL, 3, NULL, NULL, '<section id="b3-m8-c7" class="content-video-tip"> <div class="text"> <div class="title"> PARA EL MOMENTO DE LA PRÁCTICA, TEN EN CUENTA LO SIGUIENTE </div></div><div class="video"> <iframe src="https://player.vimeo.com/video/212986197?title=0&byline=0&portrait=0" width="100%" height="200" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe> </div><go-to-anchor delegate="module-handle" target="b3-m8-c8"></go-to-anchor></section>', '2017-04-27 19:12:46', '2017-04-27 19:12:46', 7, 1),
(89, 'Content 3.8', 11, NULL, NULL, 1, NULL, NULL, '<section id="b3-m8-c8" class="content-pdf"> <div class="text"> <div class="content"> CONOCE Y MEMORIZA EL GUIÓN DE ESTE MÓDULO </div></div><div> <button-pdf content="r"></button-pdf> </div><go-to-anchor delegate="module-handle" target="b3-m8-c9"></go-to-anchor></section>', '2017-04-27 19:12:46', '2017-04-27 19:12:46', 8, 1),
(90, 'Quiz 10', 11, NULL, NULL, 4, NULL, NULL, '<section id="b3-m8-c9" class="content-exam"> <div class="text"> <div class="exam-title text-uppercase"> QUIZ 10 </div><div class="exam-separator"></div><div class="exam-description"> Porque un módulo sin comprobación es como una cerveza sin cortar, refuerza tus conocimientos con este quiz. </div></div><div class="button-begin-exam padding"> <button class="button button-dark button-full text-uppercase" exam content="r">Empezar</button> </div></section>', '2017-04-27 19:12:46', '2017-04-27 19:12:46', 9, 1),
(91, 'Content 3.9', 12, NULL, NULL, 1, NULL, NULL, '<section id="b3-m9-c1" class="content-text intro"> <div class="text"> <div class="title"> <p class="bigger"> Servir la Heineken perfecta puede ser fácil si seguimos el ritual Star Serve con detalle. </p></div><div class="content"> <p> ¿Que debo recordar siempre para servir la Heineken perfecta? </p></div></div><div class="image"> <img src="main/assets/images/c/b3/m9/1.png" alt="" height="190"> </div><go-to-anchor delegate="module-handle" target="b3-m9-c2"></go-to-anchor></section>', '2017-04-27 19:12:46', '2017-04-27 19:12:46', 1, 1),
(92, 'Content 3.9', 12, NULL, NULL, 1, NULL, NULL, '<section id="b3-m9-c2" class="content-text"> <div class="text"> <div class="title"> LOS 5 PASOS DEL RITUAL STAR SERVE </div><div class="content"> <span class="text-bold">Enjuagar</span> asegurar cristalería fría y de la marca. <br><span class="text-bold">Servir</span> la cerveza en un ángulo de 45 grados. <br><span class="text-bold">Cortar</span> la cerveza cuando empieza a subir la espuma. <br><span class="text-bold">Revisar</span> que la línea de espuma esté descansando sobre la línea de la estrella. <br><span class="text-bold">Presentar</span> la cerveza con su portavasos, con el logo hacia el cliente, mientras le dice al cliente “Disfrute su Heineken” <br></div></div><div class="image"> <img src="main/assets/images/c/b3/m9/2.png" alt="" height="180"> </div><go-to-anchor delegate="module-handle" target="b3-m9-c3"></go-to-anchor></section>', '2017-04-27 19:12:46', '2017-04-27 19:12:46', 2, 1),
(93, 'Content 3.9', 12, NULL, NULL, 1, NULL, NULL, '<section id="b3-m9-c3" class="content-text"> <div class="text"> <div class="title"> LA CERVEZA DE REFERENCIA </div><div class="content"> <p> Seguramente la primer cerveza que sirva no será la mejor. Tiene que tomar esta cerveza como referencia y analizar las oportunidades de mejora para la próxima cerveza que sirva. </p></div></div><div class="image"> <img src="main/assets/images/c/b3/m9/3.png" alt="" height="180"> </div><go-to-anchor delegate="module-handle" target="b3-m9-c4"></go-to-anchor></section>', '2017-04-27 19:12:46', '2017-04-27 19:12:46', 3, 1),
(94, 'Content 3.9', 12, NULL, NULL, 1, NULL, NULL, '<section id="b3-m9-c4" class="content-text"> <div class="text"> <div class="title"> LA CERVEZA ES UN QUITASED </div><div class="content"> <p> Siempre recuerde que la razón principal para consumir cerveza es para quitar la sed. Al servir la cerveza perfecta, bien fría, y con la cantidad de espuma correcta, las probabilidades de que el consumidor pida otra aumentarán. </p></div></div><div class="image"> <img src="main/assets/images/c/b3/m9/4.png" alt="" height="180"> </div><go-to-anchor delegate="module-handle" target="b3-m9-c5"></go-to-anchor></section>', '2017-04-27 19:12:46', '2017-04-27 19:12:46', 4, 1),
(95, 'Content 3.9', 12, NULL, NULL, 1, NULL, NULL, '<section id="b3-m9-c5" class="content-text"> <div class="text"> <div class="title"> RETROGUSTO </div><div class="content"> <p> La cerveza no solo se degusta en el paladar, beber cerveza es toda una experiencia que sigue aún después de darle el sorbo a la bebida. </p><p> Debemos servir la cerveza perfecta para aumentar las posibilidades de que el consumidor pida otra cerveza. </p></div></div><div class="image"> <img src="main/assets/images/c/b3/m9/5.png" alt="" height="180"> </div><go-to-anchor delegate="module-handle" target="b3-m9-c6"></go-to-anchor></section>', '2017-04-27 19:12:46', '2017-04-27 19:12:46', 5, 1),
(96, 'Content 3.9', 12, NULL, NULL, 1, NULL, NULL, '<section id="b3-m9-c6" class="content-text"> <div class="text"> <div class="title"> CORTAR LA ESPUMA </div><div class="content"> <p> Un paso muy importante es desnatar la cerveza. Esto asegurará que el primer trago del consumidor no sea un trago amargo, sino fresco. </p><p> Además nos ayudará a mantener el CO2 en la cerveza y a protegerla del oxígeno. </p></div></div><div class="image"> <img src="main/assets/images/c/b3/m9/6.png" alt="" height="180"> </div><go-to-anchor delegate="module-handle" target="b3-m9-c7"></go-to-anchor></section>', '2017-04-27 19:12:46', '2017-04-27 19:12:46', 6, 1),
(97, 'Content 3.9', 12, NULL, NULL, 2, NULL, NULL, '<section id="b3-m9-c7" class="content-video"> <div class="text"> <div class="title"> LA HEINEKEN PERFECTA SERVIDA POR UN DRAUGHT MASTER </div></div><div class="video"> <iframe src="https://player.vimeo.com/video/214052579?title=0&byline=0&portrait=0" width="100%" height="230" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe> </div><go-to-anchor delegate="module-handle" target="b3-m9-c8"></go-to-anchor></section>', '2017-04-27 19:12:46', '2017-04-27 19:12:46', 7, 1),
(98, 'Content 3.9', 12, NULL, NULL, 3, NULL, NULL, '<section id="b3-m9-c8" class="content-video-tip"> <div class="text"> <div class="title"> TIP: LA SEGUNDA CERVEZA DEPENDE DE LA CALIDAD DE LA PRIMERA </div></div><div class="video"> <iframe src="https://player.vimeo.com/video/212986226?title=0&byline=0&portrait=0" width="100%" height="230" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe> </div><go-to-anchor delegate="module-handle" target="b3-m9-c9"></go-to-anchor></section>', '2017-04-27 19:12:46', '2017-04-27 19:12:46', 8, 1),
(99, 'Content 3.9', 12, NULL, NULL, 1, NULL, NULL, '<section id="b3-m9-c9" class="content-pdf"> <div class="text"> <div class="content"> CONOCE Y MEMORIZA EL GUIÓN DE ESTE MÓDULO </div></div><div> <button-pdf content="r"></button-pdf> </div><go-to-anchor delegate="module-handle" target="b3-m9-c10"></go-to-anchor></section>', '2017-04-27 19:12:46', '2017-04-27 19:12:46', 9, 1),
(100, 'Reto 2', 12, NULL, NULL, 4, NULL, NULL, '<section id="b3-m9-c10" class="content-exam reto"> <div class="text"> <div class="exam-title text-uppercase"> RETO 2: DEMUESTRA </div><div class="exam-separator"></div><div class="exam-description"> Es el momento que demuestres lo aprendido. </div></div><div class="button-begin-exam padding"> <button class="button button-dark button-full text-uppercase" exam content="r" challenge="true">Empezar</button> </div></section>', '2017-04-27 19:12:46', '2017-04-27 19:12:46', 10, 1),
(101, 'Content 4.1', 14, NULL, NULL, 1, NULL, NULL, '<section id="b4-m1-c1" class="content-text evidence evidence-1"> <div class="text"> <div class="title"> Has completado los módulos de entrenamiento. </div><div class="content"> <p> Pero recuerda que debes obtener calificación STAR en los todos los quizes y retos para obtener tu invitación a certificarte de manera presencial. <br/>De ser así, pronto recibirás en tu correo electrónico dicha invitación con los detalles para tu asistencia a dicha prueba.</p></div></div><go-to-anchor delegate="module-handle" target="b4-m1-c2"></go-to-anchor></section>', '2017-05-03 14:03:09', '2017-05-03 14:03:09', 1, 1),
(102, 'Content 4.1', 14, NULL, NULL, 1, NULL, NULL, '<section id="b4-m1-c2" class="content-text evidence evidence-2"> <div class="text"> <div class="title text-center"> ¿EN QU&Eacute; CONSISTE LA PRUEBA? </div><div class="content"> <p> <span class="text-bold">Día 1:</span> Entrenamiento y coaching con un certified Draught Master. </p><p> <span class="text-bold">Día 2:</span> Demostración de tus habilidades de presentación. </p></div></div><go-to-anchor delegate="module-handle" target="b4-m1-c3"></go-to-anchor></section>', '2017-05-03 14:03:09', '2017-05-03 14:03:09', 2, 1),
(103, 'Content 4.1', 14, NULL, NULL, 1, NULL, NULL, '<section id="b4-m1-c3" class="content-text evidence evidence-2"> <div class="text"> <div class="title text-center"> CRITERIOS DE EVALUACIÓN </div><div class="content text-center"> <p> Este documento te indica los rubros en los que serás evaluado el día de tu prueba presencial. Consúltalo para que estés bien seguro de que dominas cada uno de ellos. </p></div></div><div> <button-pdf content="r" text="ENVIAR LA REVIEW FORM A TU CORREO ELECTRÓNICO"></button-pdf> </div><go-to-anchor delegate="module-handle" target="b4-m1-c4"></go-to-anchor></section>', '2017-05-03 14:04:15', '2017-05-03 14:04:15', 3, 1),
(104, 'Content 4.1', 14, NULL, NULL, 1, NULL, NULL, '<section id="b4-m1-c4" class="content-text evidence evidence-1"> <div class="text"> <div class="title text-center"> <p>RECUERDA QUE LA PRÁCTICA HACE AL MAESTRO. PREPÁRATE PARA TU PRESENTACIÓN.</p><br><br><p>¡MUCHO ÉXITO!</p></div></div><div class="button-gotit"> <button class="button button-balanced button-full" ui-sref="main.course.detail({course_id: 1})"> <span class="text-uppercase">Entendido</span> </button> </div></section>', '2017-05-03 14:04:15', '2017-05-03 14:04:15', 4, 1),
(105, 'Content 6.1', 13, NULL, NULL, 1, NULL, NULL, '<section id="b6-m1-c1" class="content-text intro"> <div class="text"> <div class="title"> <p class="bigger"> AHORA QUE HAS REVISADO CADA MÓDULO DEL CURSO, TE RECOMENDAMOS VER EL VIDEO DRAUGHT MASTER CLASS SIN CORTES PARA QUE NO SE TE ESCAPE NINGÚN DETALLE </p></div></div><div class="video"> <iframe src="https://player.vimeo.com/video/213693337?title=0&byline=0&portrait=0" width="100%" height="230" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe> </div><go-to-anchor delegate="module-handle" target="b6-m1-c2"></go-to-anchor></section>', '2017-05-03 14:42:24', '2017-05-03 14:42:24', 1, 1),
(106, 'Content 6.1', 13, NULL, NULL, 1, NULL, NULL, '<section id="b6-m1-c2" class="content-text intro"> <div class="text"> <div class="title"> <span class="bigger">CÓMO REALIZAR EL STAR SERVE</span> </div><div class="content"> <p> Sabemos que dominas el Star Serve, pero tómate 3 minutos para darle una repasada con este video. </p></div></div><div class="video"> <iframe src="https://player.vimeo.com/video/213598390?title=0&byline=0&portrait=0" width="100%" height="230" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe> </div><go-to-anchor delegate="module-handle" target="b6-m1-c3"></go-to-anchor></section>', '2017-05-03 14:42:24', '2017-05-03 14:42:24', 2, 1),
(107, 'Content 6.1', 13, NULL, NULL, 1, NULL, NULL, '<section id="b6-m1-c3" class="content-text-pdf"> <div class="text"> <div class="title"> MANUAL DRAUGHT MASTER </div><div class="content"> <p> Cuando ves, escuchas y lees, tu aprendizaje es mucho mejor. Estudia este manual que contiene todo el Draught Master Training Programme por escrito. </p></div></div><div> <button-pdf content="r" text="ENVIAR EL MANUAL COMPLETO TU CORREO"></button-pdf> </div><go-to-anchor delegate="module-handle" target="b6-m1-c4"></go-to-anchor></section>', '2017-05-03 14:44:09', '2017-05-03 14:44:09', 3, 1),
(108, 'Content 6.1', 13, NULL, NULL, 1, NULL, NULL, '<section id="b6-m1-c4" class="content-text-pdf"> <div class="text"> <div class="title"> DRAUGHT MASTER REVIEW FORM </div><div class="content"> <p> Este documento te indica los rubros en los que serás evaluado el día de tu prueba presencial. Consúltalo para que estés bien seguro de que dominas cada uno de ellos. </p></div></div><div> <button-pdf content="r" text="ENVÍAR LA REVIEW FORM A TU CORREO ELECTRÓNICO"></button-pdf> </div></section>', '2017-05-03 14:44:09', '2017-05-03 14:44:09', 4, 1),
(109, 'Content 5.1', 15, NULL, NULL, 1, NULL, NULL, '<section id="b5-m1-c1" class="content-text evidence evidence-1"> <div class="text"> <div class="title">UN DRAUGHT MASTER NO DEJA DE PREPARARSE</div><div class="content"> <p> Recuerda que la práctica hace al maestro; este bloque es un seguimiento a tu entrenamiento. <br/> <span class="text-uppercase"> PARA ESTE RETO REQUIERES DEL APOYO DE TU TRIPULACIÓN Y SEGUIR LAS SIGUIENTES INSTRUCCIONES </span> </p></div></div><go-to-anchor delegate="module-handle" target="b5-m1-c2"></go-to-anchor></section>', '2017-05-12 11:50:10', '2017-05-12 11:50:10', 1, 1),
(110, 'Reto seguimiento 1', 15, NULL, NULL, 4, NULL, NULL, '<section id="b5-m1-c2" class="content-exam reto"> <div class="text"> <div class="exam-title text-uppercase"> RETO DE SEGUIMIENTO 1 </div><div class="exam-description"> Envía un video simulando la presentación completa de un entrenamiento Star Serve al staff de un bar. </div></div><div class="button-begin-exam padding"> <button class="button button-dark button-full text-uppercase" exam content="r" challenge="true">Empezar</button> </div></section>', '2017-05-12 11:50:10', '2017-05-12 11:50:10', 2, 1),
(111, 'Content 5.2', 16, NULL, NULL, 1, NULL, NULL, '<section id="b5-m2-c1" class="content-text evidence evidence-1"> <div class="text"> <div class="title">UN DRAUGHT MASTER DEBE PERFECCIONAR SU FORMA DE TRANSMITIR CONOCIMIENTO A SUS TRIPULACIONES.</div><div class="content"> <p> Recuerda que la práctica hace al maestro; este bloque es un seguimiento a tu entrenamiento. </p></div></div><go-to-anchor delegate="module-handle" target="b5-m2-c2"></go-to-anchor></section>', '2017-05-12 11:51:38', '2017-05-12 11:51:38', 1, 1),
(112, 'Reto seguimiento 2', 16, NULL, NULL, 4, NULL, NULL, '<section id="b5-m1-c2" class="content-exam reto"> <div class="text"> <div class="exam-title text-uppercase"> RETO DE SEGUIMIENTO 2 </div><div class="exam-description"> Envía un video simulando la presentación completa de un entrenamiento Star Serve al staff de un bar. </div></div><div class="button-begin-exam padding"> <button class="button button-dark button-full text-uppercase" exam content="r" challenge="true">Empezar</button> </div></section>', '2017-05-12 11:51:38', '2017-05-12 11:51:38', 2, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `content_type`
--

CREATE TABLE `content_type` (
  `id` int(11) NOT NULL,
  `name` varchar(45) NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `content_type`
--

INSERT INTO `content_type` (`id`, `name`, `active`) VALUES
(1, 'Texto', 1),
(2, 'Video', 1),
(3, 'Videotip', 1),
(4, 'Exámen', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `course`
--

CREATE TABLE `course` (
  `id` int(11) NOT NULL,
  `name` varchar(45) NOT NULL,
  `init_date` datetime DEFAULT NULL,
  `end_date` datetime DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `course`
--

INSERT INTO `course` (`id`, `name`, `init_date`, `end_date`, `created_at`, `updated_at`, `active`) VALUES
(1, 'Programa de entrenamiento Draught Master', '2017-02-20 00:00:00', '2017-05-20 00:00:00', '2017-02-20 00:00:00', '2017-02-20 00:00:00', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `diploma`
--

CREATE TABLE `diploma` (
  `id` int(11) NOT NULL,
  `course_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `pdf_url` varchar(100) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `exam_answer_type`
--

CREATE TABLE `exam_answer_type` (
  `id` int(11) NOT NULL,
  `name` varchar(45) DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `exam_answer_type`
--

INSERT INTO `exam_answer_type` (`id`, `name`, `active`) VALUES
(1, 'Texto', 1),
(2, 'Opcion multiple', 1),
(3, 'imagen', 1),
(4, 'video', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `exam_attempt`
--

CREATE TABLE `exam_attempt` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `grade` double DEFAULT NULL,
  `is_valid` tinyint(1) NOT NULL DEFAULT '0',
  `admin_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `exam_question`
--

CREATE TABLE `exam_question` (
  `id` int(11) NOT NULL,
  `question` text,
  `answer1` varchar(200) DEFAULT NULL,
  `answer2` varchar(200) DEFAULT NULL,
  `answer3` varchar(200) DEFAULT NULL,
  `answer4` varchar(200) DEFAULT NULL,
  `exam_answer_type_id` int(11) NOT NULL,
  `content_id` int(11) NOT NULL,
  `correct_answer_id` enum('1','2','3','4') DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `active` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `exam_question`
--

INSERT INTO `exam_question` (`id`, `question`, `answer1`, `answer2`, `answer3`, `answer4`, `exam_answer_type_id`, `content_id`, `correct_answer_id`, `created_at`, `updated_at`, `active`) VALUES
(1, '1. ¿En dónde se originó la cerveza como la conocemos hoy en día?', 'A. Japón', 'B. Europa', 'C. África', 'D. Medio Oriente', 2, 13, '4', '2017-05-02 11:31:52', '2017-05-02 11:31:52', 1),
(2, '2. ¿Por qué en alguna época era más sano tomar cerveza que agua?', 'A. Porque es necesario hervir el agua para preparar cerveza ', 'B. Porque el agua estaba contaminada', 'C. Porque la cerveza es una bebida pura libre de bacterias', 'D. TODAS LAS ANTERIORES', 2, 13, '4', '2017-05-02 11:31:52', '2017-05-02 11:31:52', 1),
(3, '3. ¿Cuál NO fue un beneficio que se generó en la cerveza al comenzar a incluir lúpulo en su elaboración?', 'A. Se creó un líquido más claro', 'B. Se logró mayor estabilidad en el nivel de alcohol', 'C. Se generó una mayor variedad de cervezas', 'D. La cerveza se empezó a preservar mejor', 2, 13, '2', '2017-05-02 11:31:52', '2017-05-02 11:31:52', 1),
(4, '4. ¿De dónde proviene la palabra “cerveza”?', 'A. De los monasterios en Europa', 'B. De la diosa romana de los cereales ', 'C. De la palabra griega que significa “beber”', 'D. De la diosa griega Ceres', 2, 13, '2', '2017-05-02 11:31:52', '2017-05-02 11:31:52', 1),
(5, '5. ¿En qué etapa del proceso de elaboración de cerveza se obtienen los azúcares para convertir en alcohol?', 'A. Fermentación', 'B. Maceración', 'C. Mosto', 'D. Malteo', 2, 13, '4', '2017-05-02 11:31:52', '2017-05-02 11:31:52', 1),
(6, '1. ¿Quién fue el Sr. Heineken que detonó la expansión de la marca por todo el mundo?', 'A. Adriaan', 'B. Gerard', 'C. Freddy', NULL, 2, 23, '3', '2017-05-02 11:31:52', '2017-05-02 11:31:52', 1),
(7, '2. La receta de una Heineken mantiene igual año con año con el fin de mantener el mismo sabor siempre. ¿Falso o verdadero?', 'Falso', 'Verdadero', NULL, NULL, 2, 23, '1', '2017-05-02 11:31:52', '2017-05-02 11:31:52', 1),
(8, '3. ¿Cuales son los ingredientes que tiene una Heineken en su elaboración?', 'A. AGUA, CO2 Y CEBADA', 'B. AGUA, LÚPULO Y CEBADA', 'C. AGUA, LÚPULO, CEBADA Y ADJUNTOS', 'D. AGUA, LÚPULO, TRIGO Y CEBADA', 2, 23, '2', '2017-05-02 11:31:52', '2017-05-02 11:31:52', 1),
(9, '4. ¿Cuál es el nombre de la primer cervecería que que Mr. Heineken compró y que dió origen a Heineken?', 'A. Brau Union', 'B. Haystack', 'C. Dreher', 'D. Scotish&Newcastle', 2, 23, '2', '2017-05-02 11:31:52', '2017-05-02 11:31:52', 1),
(10, '5. ¿Desde qué año se dice que Heineken está de “gira”?', 'A. 1873', 'B. 1889', 'C. 1901', 'D. 1872', 2, 23, '1', '2017-05-02 11:31:52', '2017-05-02 11:31:52', 1),
(11, '1. ¿Cual es la relación entre las irritaciones y la espuma de la cerveza?', 'A. Mientras más irritaciones ', 'B. Que todas las “irritaciones” se unen en la cabeza de la espuma', 'C. Que una cerveza mal servida genera espuma que llega a irritar la garganta del consumidor', NULL, 2, 40, '2', '2017-05-02 11:31:52', '2017-05-02 11:31:52', 1),
(12, '2. ¿Porque utilizamos un portavasos cuando presentamos la cerveza?', 'A. Es un elemento de servicio que ayuda a proteger al consumidor', 'B. Porque se ve bonito', 'C. Porque es parte del ritual de servido', NULL, 2, 40, '1', '2017-05-02 11:31:52', '2017-05-02 11:31:52', 1),
(13, '3. ¿Cúal es la temperatura correcta para servir una Heineken? ', 'A. Al tiempo', 'B. Tibia', 'C. Fría', NULL, 2, 40, '3', '2017-05-02 11:31:52', '2017-05-02 11:31:52', 1),
(14, '4. ¿Cuales son las principales molestias que puede tener un cliente al recibir una cerveza?', 'A. Cristalería limpia, con mucha espuma y servido con portavasos', 'B. El precio de la cerveza y que el mesero la sirva con mala cara', 'C. La cantidad de espuma, temperatura de la cerveza, vaso sucio y mala presentacion', NULL, 2, 40, '3', '2017-05-02 11:31:52', '2017-05-02 11:31:52', 1),
(15, '5. ¿Cuál es la razón principal para evitar causar molestias  al cliente al momento de servirle una cerveza?', 'A. Para aumentar la probabilidad de que el cliente me pida otra cerveza ', 'B. Para evitar que el cliente se moleste y me grite', 'C. Para presumir al cliente  que yo soy un Draught Ambassador', NULL, 2, 40, '1', '2017-05-02 11:31:52', '2017-05-02 11:31:52', 1),
(16, '1.  ¿Qué información nos puede decir la cerveza de referencia?', 'A. La presión de la máquina', 'B. Información sobre el equipo de barril con el que contamos', 'C. B y D son correctas', 'D. Mi técnica de servido', 2, 47, '3', '2017-05-02 11:31:52', '2017-05-02 11:31:52', 1),
(17, '2. ¿Cómo se llama a la primera cerveza que servimos con un frente a una audiencia repleta de desconocidos?', 'A. Cerveza de apoyo', 'B. Star Serve', 'C. La hora de la verdad', 'D. Cerveza de referencia', 2, 47, '4', '2017-05-02 11:31:52', '2017-05-02 11:31:52', 1),
(18, '3. ¿Qué es lo que se le pide al participante antes de que sirva su segunda cerveza?', 'A. Que determine el objetivo de su segunda cerveza', 'B. Pedir feedback a un compañero', 'C. Que se relaje y se sacuda un poco', 'D. Que lea el manual de Star Serve', 2, 47, '1', '2017-05-02 11:31:52', '2017-05-02 11:31:52', 1),
(19, '4. ¿Cuál es el objetivo de que un participante sirva su una cerveza sin previo aviso?', 'A. Que la audiencia reconozca mis conocimientos en Star Serve', 'B. Involucrar a los participantes', 'C. Explicar lo que podemos aprender de la primer cerveza', 'D. Tener una idea del nivel de habilidades que tienen los participantes', 2, 47, '3', '2017-05-02 11:31:52', '2017-05-02 11:31:52', 1),
(20, '5. Si pasamos al frente a 4 personas notaremos distintos estilos de servido.', 'Falso', 'Verdadero', NULL, NULL, 2, 47, '2', '2017-05-02 11:31:52', '2017-05-02 11:31:52', 1),
(21, '1. ¿Cuál es la impresión que tus participantes deben tener sobre El Ritual al observarte hacerlo?', 'A. Que no es tan fácil como parece', 'B. Que se sientan ansiosos por intentar el servido', 'C. Hacer que parezca fácil', 'D. Demostrar que algunos no son aptos y se requiere entrenamiento', 2, 57, '3', '2017-05-02 11:31:52', '2017-05-02 11:31:52', 1),
(22, '2. ¿Cuál es el tercer  paso del ritual?', 'A. Cortar', 'B. Abrir la llave', 'C. Servir', 'D. Enjuagar', 2, 57, '1', '2017-05-02 11:31:52', '2017-05-02 11:31:52', 1),
(23, '3. ¿Cuál es un buen indicador de que el vaso está limpio?', 'A. No hay uno en realidad, se debe lavar justo antes de servir', 'B. La cerveza se ve opaca', 'C. El agua no se adhiere bien a las paredes del vaso, dejando espacios secos', 'D. La espuma se vuelve más amarga de lo normal', 2, 57, '3', '2017-05-02 11:31:52', '2017-05-02 11:31:52', 1),
(24, '4. ¿Cual es la inclinación correcta para colocar el vaso bajo el grifo?', 'A. Depende del vaso y de la resistencia del vidrio', 'B. 45 grados y enderezar antes de que derrame', 'C. 5 grados de diferencia al inclinar el vaso', 'D. Si es un barman zurdo se recomienda 45 grados', 2, 57, '2', '2017-05-02 11:31:52', '2017-05-02 11:31:52', 1),
(25, '5. ¿Qué debes hacer al servir tu Heineken frente al grupo?', 'A. Cometer algún error para que ellos se motiven a participar', 'B. Servir una Heineken perfecta, sin errores', 'C. Nunca perder contacto visual con los participantes', 'D. Preguntar sobre los pasos aprendidos', 2, 57, '2', '2017-05-02 11:31:52', '2017-05-02 11:31:52', 1),
(26, '6. ¿Que se busca en el cuarto paso del Ritual de servido?', 'A. Inclinar el vaso a 45 y abrir el grifo con firmeza', 'B. Que el logo esté viendo al cliente', 'C. Quitar la espuma más amarga y crear una capa delgada de agua', 'D. Que la espuma esté descansando sobre los hombros de la estrella', 2, 57, '4', '2017-05-02 11:31:52', '2017-05-02 11:31:52', 1),
(27, '1.  ¿Qué le pasa a la cerveza cuando entra en contacto con el vaso?', 'A. Se oxida y contamina', 'B. La presión del choque la convierte en espuma', 'C. Pierde sus propiedades', 'D. Se convierte en espuma y se evapora', 2, 65, '2', '2017-05-02 11:31:52', '2017-05-02 11:31:52', 1),
(28, '2. ¿Qué pasa si dejamos reposar por unos momentos un vaso con mucha espuma?', 'A. El dióxido de carbono  convierte la espuma en líquido ', 'B. Desaparece porque se evapora', 'C. La espuma se convierte en cerveza ', 'D. Nada', 2, 65, '3', '2017-05-02 11:31:52', '2017-05-02 11:31:52', 1),
(29, '3. ¿Qué sucede cuando el oxígeno entra en la espuma?', 'A. La hace colapsar', 'B. Hace que tome un color más opaco', 'C. Prolonga su estado', 'D. Evita que siga subiendo la espuma', 2, 65, '1', '2017-05-02 11:31:52', '2017-05-02 11:31:52', 1),
(30, '4. ¿Por qué queremos la cantidad correcta de espuma en la cerveza?', 'A. Para protegerla de la oxidación y mantener el CO2 adentro, mejorando la tomabilidad', 'B. Porque es lo indicado en el ritual de servido de Heineken', 'C. Porque es como se presenta en los anuncios y se vende mejor', 'D. Porque el CO2 hace provoca un efecto dominó en las burbujas y evita que se rompan', 2, 65, '1', '2017-05-02 11:31:52', '2017-05-02 11:31:52', 1),
(31, '1. ¿Desde hace cuanto tiempo se toma cerveza? ¿Y los Refrescos?', 'A. Cerveza: Mil años \nRefrescos; Dos mil años', 'B. La cerveza y los refrescos se descubrieron el mismo día', 'C. Cerveza; 10,000 mil \nRefrescos; 500 años', 'D. Cerveza: 5,000 mil \nRefrescos:  10,000 años\n', 2, 73, '3', '2017-05-02 11:31:52', '2017-05-02 11:31:52', 1),
(32, '2. ¿Porque la humanidad lleva tantos años tomando cerveza?', 'A. Porque es divertida', 'B. Porque es un buen quitased', 'C. Porque es un vicio', 'D. Porque la fabricaban los monjes', 2, 73, '2', '2017-05-02 11:31:52', '2017-05-02 11:31:52', 1),
(33, '3. ¿Cuales son las características de un buen quita sed?', 'A. Frío, Dulce y Refrescante', 'B. Líquido, Frío, Carbonatado y un poco Amargo', 'C. Frío, Limpio, con espuma y bien presentado', 'D. Que contenga un poco de Co2 para que se pueda digerir mejor', 2, 73, '2', '2017-05-02 11:31:52', '2017-05-02 11:31:52', 1),
(34, '4. ¿El CO2 es responsable de la carbonatación de la cereza, característica que hace que la cerveza engorde?', 'A. Verdadero, El CO2 provoca gases y engorda', 'B. Falso, el CO2 facilita la digestión y hace la cerveza más tomable', NULL, NULL, 2, 73, '2', '2017-05-02 11:31:52', '2017-05-02 11:31:52', 1),
(35, '1. ¿Cuál es una razón poderosa que tus participantes deben entender acerca de porqué hacer el Star Serve?', 'A. Porque el servido representa a la marca', 'B. Porque habla de un bartender diferenciado de los demás', 'C. Porque existen incentivos a quién cumpla el ritual', 'D. Se aumenta la capacidad de beber el producto', 2, 81, '4', '2017-05-02 11:31:52', '2017-05-02 11:31:52', 1),
(36, '2. ¿La espuma es importante, no importa la técnica que se utilizó para generarla?', 'Verdadero', 'Falso', NULL, NULL, 2, 81, '2', '2017-05-02 11:31:52', '2017-05-02 11:31:52', 1),
(37, '3. ¿Que es más importante cuando tomamos cerveza, el sabor o el “sabor que deja”?', 'A. El sabor, debe estar en punto exacto de amargor', 'B. El sabor que deja, que provoca decir “Ahh” y querer más ', 'C. No se puede definir que es más importante, depende del tomador', NULL, 2, 81, '2', '2017-05-02 11:31:52', '2017-05-02 11:31:52', 1),
(38, '4. ¿Cuál es una razón poderosa que tus participantes deben entender acerca de porqué hacer el Star Serve?', 'A. Porque el servido representa a la marca', 'B. Porque habla de un bartender diferenciado de los demás', 'C. Porque existen incentivos a quién cumpla el ritual', 'D. Se aumenta la capacidad de beber el producto', 2, 81, '4', '2017-05-02 11:31:52', '2017-05-02 11:31:52', 1),
(39, '1. Es una característica de una cerveza cortada', 'A. Visualmente es más bonita', 'B. La espuma dura más porque se retiran las burbujas grandes que provocan efecto dominó al resto', 'C. Es menos amarga. Retiramos la espuma que contiene el lúpulo más amargo', 'D. Todas las anteriores', 2, 90, '4', '2017-05-02 11:31:52', '2017-05-02 11:31:52', 1),
(40, '2. La espuma que cortamos contiene la parte más ligera del lúpulo, conocida como ácidos iso alfa, que son los que provocan la deshidratación y la cruda', 'Falso', 'Verdadero', NULL, NULL, 2, 90, '2', '2017-05-02 11:31:52', '2017-05-02 11:31:52', 1),
(41, '3. ¿Por qué cortamos a 45 grados?', 'A. Porque es la misma inclinación del vaso', 'B. Porque queremos lograr una cubierta de agua como cupula', 'C. Para retirar la espuma más amarga de manera que salga del vaso y no regrese', 'C. Para generar una capa más dura que soporte una espátula', 2, 90, '3', '2017-05-02 11:31:52', '2017-05-02 11:31:52', 1),
(42, '4.¿Por qué el skimmer debe de estar mojado?', 'A. Para asegurar la limpieza de la espátula', 'B. Para generar una cubierta de agua que haga más resistente la espuma', 'C. Ayuda a retirar el lúpulo más ácido, y mejorar el sabor de la cerveza', 'D. Se aumenta la capacidad de beber el producto', 2, 90, '2', '2017-05-02 11:31:52', '2017-05-02 11:31:52', 1),
(43, 'Reto 1. Presentante:\r\n\r\nEscribe un texto de una \r\ncuartilla con el guión de cómo sería tu presentación ante un grupo de participantes a la sesión de capacitación Star Serve.\r\n\r\nNo olvides incluir tu nombre, tus credenciales y el objetivo de la sesión.\r\n', NULL, NULL, NULL, NULL, 1, 31, NULL, '2017-05-02 13:32:33', '2017-05-02 13:32:33', 1),
(44, 'Envía un video simulando la presentación completa de un entrenamiento Star Serve al staff de un bar. Grábate dando cada uno de los módulos a una audiencia imaginaria, o puedes pedir apoyo a algunos compañeros de tu región. No es necesario que realices el servido con en la torre, solo que nos demuestres que te sabes el guión de entrenamiento y que estás listo para la sesión de práctica y certificación “The Evidence”.', NULL, NULL, NULL, NULL, 4, 100, NULL, '2017-05-02 16:53:47', '2017-05-02 16:53:47', 1),
(45, 'Grábate dando cada uno de los módulos a una audiencia imaginaria, o puedes pedir apoyo a algunos compañeros de tu región.\r\n\r\nNo es necesario que realices el servido con en la torre, solo que nos demuestres que te sabes el guión de entrenamiento y que estás listo para la sesión de práctica y certificación “The Evidence”.', NULL, NULL, NULL, NULL, 4, 110, NULL, '2017-05-12 11:54:44', '2017-05-12 11:54:44', 1),
(46, 'Grábate dando cada uno de los módulos a una audiencia imaginaria, o puedes pedir apoyo a algunos compañeros de tu región.\r\n\r\nNo es necesario que realices el servido con en la torre, solo que nos demuestres que te sabes el guión de entrenamiento y que estás listo para la sesión de práctica y certificación “The Evidence”.', NULL, NULL, NULL, NULL, 4, 112, NULL, '2017-05-12 11:55:09', '2017-05-12 11:55:09', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `group`
--

CREATE TABLE `group` (
  `id` int(11) NOT NULL,
  `name` varchar(45) NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `group_course`
--

CREATE TABLE `group_course` (
  `course_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `module`
--

CREATE TABLE `module` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `description` varchar(150) DEFAULT NULL,
  `block_id` int(11) NOT NULL,
  `init_date` datetime DEFAULT NULL,
  `end_date` datetime DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `order` int(11) NOT NULL DEFAULT '0',
  `active` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `module`
--

INSERT INTO `module` (`id`, `name`, `description`, `block_id`, `init_date`, `end_date`, `created_at`, `updated_at`, `order`, `active`) VALUES
(1, 'Módulo 1: The welcome', NULL, 1, NULL, NULL, '2017-02-20 00:00:00', '2017-02-20 00:00:00', 1, 1),
(2, 'Módulo 1: Historia de la cerveza', 'Hay datos históricos de la cerveza que te sorprenderán a ti y a tus próximos pupilos.', 2, NULL, NULL, '2017-02-21 00:00:00', '2017-02-21 00:00:00', 1, 1),
(3, 'Módulo 2: Historia de Heineken', '"Vendedor de emociones" un término que tendrá más sentido después de cursar este módulo.', 2, NULL, NULL, '2017-02-21 00:00:00', '2017-02-21 00:00:00', 2, 1),
(4, 'Módulo 1: La Presentación', 'Iniciemos con tu preparación de presentaciones efectivas.', 3, NULL, NULL, '2017-03-27 18:43:42', '2017-03-27 18:43:42', 1, 1),
(5, 'Módulo 2: Las irritaciones', 'Las áreas problemáticas que identificarán tus participantes.', 3, NULL, NULL, '2017-03-27 18:43:42', '2017-03-27 18:43:42', 2, 1),
(6, 'Módulo 3: Cerveza de referencia', 'Lo que aprendemos al servir una cerveza sin aviso alguno.', 3, NULL, NULL, '2017-03-27 18:44:50', '2017-03-27 18:44:50', 3, 1),
(7, 'Módulo 4: El Ritual de Servicio', '¿Por qué hacerlo?', 3, NULL, NULL, '2017-03-27 18:44:50', '2017-03-27 18:44:50', 4, 1),
(8, 'Módulo 5: ¿Con qué estamos trabajando?', 'Secretos de la espuma.', 3, NULL, NULL, '2017-03-27 19:03:57', '2017-03-27 19:03:57', 5, 1),
(9, 'Módulo 6: Historia y Cualidades de un Quitased', 'Eres el enlace entre el Star Serve y sus beneficios para los participantes.', 3, NULL, NULL, '2017-03-27 19:03:57', '2017-03-27 19:03:57', 6, 1),
(10, 'Módulo 7: Gusto y retrogusto', 'La importancia del sabor residual.', 3, NULL, NULL, '2017-03-27 19:11:58', '2017-03-27 19:11:58', 7, 1),
(11, 'Módulo 8: Cortar o no cortar', 'La diferencia entre la cerveza desnatada y la cerveza sin cáscara.', 3, NULL, NULL, '2017-03-27 19:11:58', '2017-03-27 19:11:58', 8, 1),
(12, 'Módulo 9: El cierre', '¿Estas listo para servir una Heineken?', 3, NULL, NULL, '2017-03-27 19:12:28', '2017-03-27 19:12:28', 9, 1),
(13, 'Módulo 1: Support', NULL, 6, NULL, NULL, '2017-05-03 10:33:08', '2017-05-03 10:33:08', 1, 1),
(14, 'Módulo 1. The evidence', NULL, 4, NULL, NULL, '2017-05-03 13:55:36', '2017-05-03 13:55:36', 1, 1),
(15, 'Reto de seguimiento 1', NULL, 5, NULL, NULL, '2017-05-12 11:47:39', '2017-05-12 11:47:39', 1, 1),
(16, 'Reto de seguimiento 2', NULL, 5, NULL, NULL, '2017-05-12 11:48:03', '2017-05-12 11:48:03', 2, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `notification`
--

CREATE TABLE `notification` (
  `id` int(11) NOT NULL,
  `message` varchar(100) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `notification_user`
--

CREATE TABLE `notification_user` (
  `notification_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `profile`
--

CREATE TABLE `profile` (
  `id` int(11) NOT NULL,
  `name` varchar(45) NOT NULL,
  `is_admin` tinyint(1) NOT NULL DEFAULT '0',
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `profile`
--

INSERT INTO `profile` (`id`, `name`, `is_admin`, `active`, `created_at`, `updated_at`) VALUES
(1, 'Admin', 1, 1, '2017-02-20 00:00:00', '2017-02-20 00:00:00'),
(2, 'User', 0, 1, '2017-02-20 00:00:00', '2017-02-20 00:00:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `related_content`
--

CREATE TABLE `related_content` (
  `id` int(11) NOT NULL,
  `name` varchar(45) DEFAULT NULL,
  `content_pdf` varchar(100) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `status`
--

CREATE TABLE `status` (
  `id` int(11) NOT NULL,
  `course_id` int(11) NOT NULL,
  `name` varchar(45) NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `name` varchar(45) NOT NULL,
  `lastname` varchar(45) NOT NULL,
  `email` varchar(45) NOT NULL,
  `username` varchar(45) NOT NULL,
  `password` varchar(100) NOT NULL,
  `profile_id` int(11) NOT NULL,
  `push_notif_token` varchar(170) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `city` varchar(100) DEFAULT NULL,
  `state` varchar(100) DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `image_url` varchar(100) DEFAULT NULL,
  `last_login` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `user`
--

INSERT INTO `user` (`id`, `name`, `lastname`, `email`, `username`, `password`, `profile_id`, `push_notif_token`, `created_at`, `updated_at`, `city`, `state`, `active`, `image_url`, `last_login`) VALUES
(1, 'Admin', 'Admin', 'admin@example.com', 'admin', '$2y$10$1kFi1t3ZmCIIRcVPeKNFzOhfCMXeKPBgngMKtoIvgeJtBEkFH5c/m', 1, NULL, '2017-04-28 12:57:55', '2017-05-15 12:14:55', NULL, NULL, 1, NULL, '2017-05-15 16:14:55');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `user_access`
--

CREATE TABLE `user_access` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `access_type_id` int(11) NOT NULL,
  `object_id` int(11) NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `locked` tinyint(1) DEFAULT '1',
  `done` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `user_course_status`
--

CREATE TABLE `user_course_status` (
  `user_id` int(11) NOT NULL,
  `course_id` int(11) NOT NULL,
  `status_id` int(11) NOT NULL,
  `invited` tinyint(1) NOT NULL DEFAULT '0',
  `diploma_sent` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `user_exam_answer`
--

CREATE TABLE `user_exam_answer` (
  `id` int(11) NOT NULL,
  `answer` text NOT NULL,
  `grade` double DEFAULT '0',
  `is_valid` tinyint(1) DEFAULT '0',
  `answer_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `admin_id` int(11) NOT NULL,
  `exam_attempt_id` int(11) NOT NULL,
  `exam_question_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `user_group`
--

CREATE TABLE `user_group` (
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `access_type`
--
ALTER TABLE `access_type`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `block`
--
ALTER TABLE `block`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_block_course1_idx` (`course_id`);

--
-- Indices de la tabla `content`
--
ALTER TABLE `content`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_content_content_type1_idx` (`content_type_id`),
  ADD KEY `fk_content_related_content1_idx` (`related_content_id`),
  ADD KEY `fk_content_module1_idx` (`module_id`);

--
-- Indices de la tabla `content_type`
--
ALTER TABLE `content_type`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `course`
--
ALTER TABLE `course`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `diploma`
--
ALTER TABLE `diploma`
  ADD PRIMARY KEY (`id`,`course_id`,`user_id`),
  ADD UNIQUE KEY `UNIQUE` (`course_id`,`user_id`),
  ADD KEY `fk_diploma_user1_idx` (`user_id`),
  ADD KEY `fk_diploma_course1_idx` (`course_id`);

--
-- Indices de la tabla `exam_answer_type`
--
ALTER TABLE `exam_answer_type`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `exam_attempt`
--
ALTER TABLE `exam_attempt`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_exam_attempt_user1_idx` (`user_id`);

--
-- Indices de la tabla `exam_question`
--
ALTER TABLE `exam_question`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_exam_question_exam_answer_type1_idx` (`exam_answer_type_id`),
  ADD KEY `fk_exam_question_content1_idx` (`content_id`);

--
-- Indices de la tabla `group`
--
ALTER TABLE `group`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `group_course`
--
ALTER TABLE `group_course`
  ADD PRIMARY KEY (`course_id`,`group_id`),
  ADD KEY `fk_course_has_group_group1_idx` (`group_id`),
  ADD KEY `fk_course_has_group_course1_idx` (`course_id`);

--
-- Indices de la tabla `module`
--
ALTER TABLE `module`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_module_block1_idx` (`block_id`);

--
-- Indices de la tabla `notification`
--
ALTER TABLE `notification`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_notification_user_idx` (`user_id`);

--
-- Indices de la tabla `notification_user`
--
ALTER TABLE `notification_user`
  ADD PRIMARY KEY (`notification_id`,`user_id`),
  ADD KEY `fk_notification_has_user_user1_idx` (`user_id`),
  ADD KEY `fk_notification_has_user_notification1_idx` (`notification_id`);

--
-- Indices de la tabla `profile`
--
ALTER TABLE `profile`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `related_content`
--
ALTER TABLE `related_content`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `status`
--
ALTER TABLE `status`
  ADD PRIMARY KEY (`id`,`course_id`),
  ADD KEY `fk_status_course1_idx` (`course_id`);

--
-- Indices de la tabla `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username_UNIQUE` (`username`),
  ADD KEY `fk_user_profile1_idx` (`profile_id`);

--
-- Indices de la tabla `user_access`
--
ALTER TABLE `user_access`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `user_id` (`user_id`,`access_type_id`,`object_id`) USING BTREE,
  ADD KEY `fk_user_has_exam_question_user1_idx` (`user_id`),
  ADD KEY `fk_user_access_access_type1_idx` (`access_type_id`);

--
-- Indices de la tabla `user_course_status`
--
ALTER TABLE `user_course_status`
  ADD PRIMARY KEY (`user_id`,`course_id`),
  ADD KEY `fk_user_has_course_course1_idx` (`course_id`),
  ADD KEY `fk_user_has_course_user1_idx` (`user_id`),
  ADD KEY `fk_user_course_status_status1_idx` (`status_id`);

--
-- Indices de la tabla `user_exam_answer`
--
ALTER TABLE `user_exam_answer`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_user_exam_answer_exam_attempt1_idx` (`exam_attempt_id`),
  ADD KEY `fk_user_exam_answer_exam_question1_idx` (`exam_question_id`);

--
-- Indices de la tabla `user_group`
--
ALTER TABLE `user_group`
  ADD PRIMARY KEY (`user_id`,`group_id`),
  ADD KEY `fk_user_has_group_group1_idx` (`group_id`),
  ADD KEY `fk_user_has_group_user1_idx` (`user_id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `access_type`
--
ALTER TABLE `access_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `block`
--
ALTER TABLE `block`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT de la tabla `content`
--
ALTER TABLE `content`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=113;
--
-- AUTO_INCREMENT de la tabla `content_type`
--
ALTER TABLE `content_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `course`
--
ALTER TABLE `course`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `diploma`
--
ALTER TABLE `diploma`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `exam_answer_type`
--
ALTER TABLE `exam_answer_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `exam_attempt`
--
ALTER TABLE `exam_attempt`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `exam_question`
--
ALTER TABLE `exam_question`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;
--
-- AUTO_INCREMENT de la tabla `module`
--
ALTER TABLE `module`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT de la tabla `notification`
--
ALTER TABLE `notification`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `profile`
--
ALTER TABLE `profile`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `related_content`
--
ALTER TABLE `related_content`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;
--
-- AUTO_INCREMENT de la tabla `user_access`
--
ALTER TABLE `user_access`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `user_exam_answer`
--
ALTER TABLE `user_exam_answer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `block`
--
ALTER TABLE `block`
  ADD CONSTRAINT `fk_block_course1` FOREIGN KEY (`course_id`) REFERENCES `course` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `content`
--
ALTER TABLE `content`
  ADD CONSTRAINT `fk_content_content_type1` FOREIGN KEY (`content_type_id`) REFERENCES `content_type` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_content_module1` FOREIGN KEY (`module_id`) REFERENCES `module` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_content_related_content1` FOREIGN KEY (`related_content_id`) REFERENCES `related_content` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `diploma`
--
ALTER TABLE `diploma`
  ADD CONSTRAINT `fk_diploma_course1` FOREIGN KEY (`course_id`) REFERENCES `course` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_diploma_user1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `exam_attempt`
--
ALTER TABLE `exam_attempt`
  ADD CONSTRAINT `fk_exam_attempt_user1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `exam_question`
--
ALTER TABLE `exam_question`
  ADD CONSTRAINT `fk_exam_question_content1` FOREIGN KEY (`content_id`) REFERENCES `content` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_exam_question_exam_answer_type1` FOREIGN KEY (`exam_answer_type_id`) REFERENCES `exam_answer_type` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `group_course`
--
ALTER TABLE `group_course`
  ADD CONSTRAINT `fk_course_has_group_course1` FOREIGN KEY (`course_id`) REFERENCES `course` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_course_has_group_group1` FOREIGN KEY (`group_id`) REFERENCES `group` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `module`
--
ALTER TABLE `module`
  ADD CONSTRAINT `fk_module_block1` FOREIGN KEY (`block_id`) REFERENCES `block` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `notification`
--
ALTER TABLE `notification`
  ADD CONSTRAINT `fk_notification_user` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `notification_user`
--
ALTER TABLE `notification_user`
  ADD CONSTRAINT `fk_notification_has_user_notification1` FOREIGN KEY (`notification_id`) REFERENCES `notification` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_notification_has_user_user1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `status`
--
ALTER TABLE `status`
  ADD CONSTRAINT `fk_status_course1` FOREIGN KEY (`course_id`) REFERENCES `course` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `user`
--
ALTER TABLE `user`
  ADD CONSTRAINT `fk_user_profile1` FOREIGN KEY (`profile_id`) REFERENCES `profile` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `user_access`
--
ALTER TABLE `user_access`
  ADD CONSTRAINT `fk_user_access_access_type1` FOREIGN KEY (`access_type_id`) REFERENCES `access_type` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_user_has_exam_question_user1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `user_course_status`
--
ALTER TABLE `user_course_status`
  ADD CONSTRAINT `fk_user_course_status_status1` FOREIGN KEY (`status_id`) REFERENCES `status` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_user_has_course_course1` FOREIGN KEY (`course_id`) REFERENCES `course` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_user_has_course_user1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `user_exam_answer`
--
ALTER TABLE `user_exam_answer`
  ADD CONSTRAINT `fk_user_exam_answer_exam_attempt1` FOREIGN KEY (`exam_attempt_id`) REFERENCES `exam_attempt` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_user_exam_answer_exam_question1` FOREIGN KEY (`exam_question_id`) REFERENCES `exam_question` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `user_group`
--
ALTER TABLE `user_group`
  ADD CONSTRAINT `fk_user_has_group_group1` FOREIGN KEY (`group_id`) REFERENCES `group` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_user_has_group_user1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
